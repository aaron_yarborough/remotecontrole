﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RemoteControlE
{
    public static class Helpers
    {
        public static string GetAppDataDir()
        {
            return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "RemoteControlE");
        }

        public static Color GetColorFromString(string str)
        {
            Console.WriteLine("Converting the following colour: {0}", str);

            Color toReturn;

            // Return 'Black' if there's nothing there
            if (String.IsNullOrEmpty(str))
                return Color.Black;

            // RGB
            if (str.Contains(','))
            {
                string[] splitVals = str.Split(',');
                int red = Convert.ToInt32(splitVals[0]);
                int green = Convert.ToInt32(splitVals[1]);
                int blue = Convert.ToInt32(splitVals[2]);

                Console.WriteLine("Red: {0}\tGreen: {1}\tBlue: {2}", red, green, blue);
                toReturn = Color.FromArgb(255, red, green, blue);
            }
            // Colour name
            else
            {
                toReturn = Color.FromName(str);
            }

            return toReturn;
        }
    }
}
