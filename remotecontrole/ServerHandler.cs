﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenGraal.GraalIM;

namespace RemoteControlE
{
    public static class ServerHandler
    {
        public static void ConnectToServer(string ip, int port)
        {
            Information.ServerIp = ip;
            Information.ServerPort = port;

            Framework framework = Framework.GetInstance();
            framework.RunServer();

            Information.Framework = framework;
        }
    }
}
