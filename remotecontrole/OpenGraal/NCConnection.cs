﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using OpenGraal;
using OpenGraal.Core;
using OpenGraal.GraalIM;

namespace OpenGraal.NpcServer
{
    public class NCConnection : CSocket
    {

        public event OpenGraal.GraalIM.Connections.GraalServer.MyEventHandler OnNCPacketChange;

        /// <summary>
        /// Enumerator -> Packet In
        /// </summary>

        public enum PacketIn
        {
            NC_NPCGET = 103,	// {103}{INT id}
            NC_NPCDELETE = 104,	// {104}{INT id}
            NC_NPCRESET = 105,	// {105}{INT id}
            NC_NPCSCRIPTGET = 106,	// {106}{INT id}
            NC_NPCWARP = 107,	// {107}{INT id}{CHAR x*2}{CHAR y*2}{level}
            NC_NPCFLAGSGET = 108,	// {108}{INT id}
            NC_NPCSCRIPTSET = 109,	// {109}{INT id}{GSTRING script}
            NC_NPCFLAGSSET = 110,	// {110}{INT id}{GSTRING flags}
            NC_NPCADD = 111,	// {111}{GSTRING info}  - (info) name,id,type,scripter,starting level,x,y
            NC_CLASSGET = 112,	// {112}{class}
            NC_CLASSADD = 113,	// {113}{CHAR name length}{name}{GSTRING script}
            NC_LOCALNPCSGET = 114,	// {114}{level}
            NC_WEAPONLISTGET = 115,	// {115}
            NC_WEAPONGET = 116,	// {116}{weapon}
            NC_WEAPONADD = 117,	// {117}{CHAR weapon length}{weapon}{CHAR image length}{image}{code}
            NC_WEAPONDELETE = 118,	// {118}{weapon}
            NC_CLASSDELETE = 119,	// {119}{class}
            NC_LEVELLISTGET = 150,	// {150}
            NC_LEVELLISTSET = 151,	// {151}{GSTRING levels}
            NC_CHAT = 74,
        };

        /// <summary>
        /// Enumerator -> Packet Out
        /// </summary> 

        public enum PacketOut
        {
            NC_CHAT = 74,	// {74}{GSTRING text}
            NC_LEVELLIST = 80,	// {80}{GSTRING levels}
            NC_NPCATTRIBUTES = 157,	// {157}{GSTRING attributes}
            NC_NPCADD = 158,	// {158}{INT id}{CHAR 50}{CHAR name length}{name}{CHAR 51}{CHAR type length}{type}{CHAR 52}{CHAR level length}{level}
            NC_NPCDELETE = 159,	// {159}{INT id}
            NC_NPCSCRIPT = 160,	// {160}{INT id}{GSTRING script}
            NC_NPCFLAGS = 161,	// {161}{INT id}{GSTRING flags}
            NC_CLASSGET = 162,	// {162}{CHAR name length}{name}{GSTRING script}
            NC_CLASSADD = 163,	// {163}{class}
            NC_LEVELDUMP = 164,
            NC_WEAPONLISTGET = 167,	// {167}{CHAR name1 length}{name1}{CHAR name2 length}{name2}...
            NC_CLASSDELETE = 188,	// {188}{class}
            NC_WEAPONGET = 192,	// {192}{CHAR name length}{name}{CHAR image length}{image}{script}
        };

        /// <summary>
        /// Member Variables
        /// </summary>
        //private Framework Server;
        public bool LoggedIn = false;
        public string Account, Password;
        public string newdata = "";
        public string weapon;
        /// <summary>
        /// Base Constructor
        /// </summary>
        public NCConnection()
            : base()
        {
            //Server = Framework.GetInstance();
            this.Init();
            this.Setup();

            Information.NcConnection = this;
        }

        ~NCConnection()
        {
        }

        /// <summary>
        /// Handle Login Packet
        /// </summary>
        public void SendLogin(string username, string password)
        {
            // Check Type & Version
            int type = 8;
            String version = "NCL21075";
            CString loginPacket = new CString() + (byte)type + version + (byte)username.Length + username + (byte)password.Length + password + "\n";
            SendPacket(loginPacket, true);

            //MessageBox.Show(loginPacket.ToString());
            //Abstraction.GetInstance().WriteText(loginPacket.ToString());

            this.ReceiveData();

            // Set Login
            LoggedIn = true;
            Information.Framework.ConnectedNC = true;

            this.ConnectedToNC();
        }

        public void ConnectedToNC()
        {
        }

        public void GetWeaponContent(string weapon)
        {
            this.weapon = weapon;
            Console.WriteLine("Get weapon " + weapon);
            SendPacket(new CString() + (byte)PacketIn.NC_WEAPONGET + weapon, true);
        }

        public void GetClassContent(string className)
        {
            SendPacket(new CString() + (byte)PacketIn.NC_CLASSGET + className, true);
        }

        public void GetWeaponList()
        {
            SendPacket(new CString() + (byte)PacketIn.NC_WEAPONLISTGET, true);
        }

        public int packet_id = 0;
        public CString packetdata = new CString();
        public CString wholepacket = new CString();
        /// <summary>
        /// Handle Received Data
        /// </summary>
        
        
        protected override void HandleData(CString Packet)
        {

            //Thread.Sleep(1500);

            // Player not logged in
            //if (!LoggedIn)
            //	HandleLogin(Packet.ReadString('\n'));

            // Parse Packets
            while (Packet.BytesLeft > 0)
            {
                // Grab Single Packet
                CString CurPacket = Packet.ReadString('\n');

                // Read Packet Type
                int PacketId = CurPacket.ReadGUByte1();

                // Run Internal Packet Function
                switch ((PacketOut)PacketId)
                {
                    default:
                        packet_id = PacketId;
                        newdata = CurPacket.Text;
                        packetdata = CurPacket;

                        Console.WriteLine(String.Format("NPC SERVER: {0} - {1}", packet_id, newdata));
                        break;

                    case PacketOut.NC_CHAT:
                        Information.serverWindow.serverWindowChat.UpdateRCChat(CurPacket.Text.Substring(1));
                        break;

                    case PacketOut.NC_WEAPONLISTGET:
                        List<string> weapons = new List<string>();
                        while (CurPacket.BytesLeft > 0)
                        {
                            int weaponlength = CurPacket.ReadGUByte1();
                            string name = CurPacket.ReadChars(weaponlength);
                            weapons.Add(name);
                        }

                        Information.serverWindow.serverWindowWeaponList.AddWeaponList(weapons);
                        break;

                    case PacketOut.NC_WEAPONGET:
                        int weaponNameLegth = CurPacket.ReadGUByte1();
                        string weaponName = CurPacket.ReadChars(weaponNameLegth);
                        int imageNameLength = CurPacket.ReadGUByte1();
                        string imageName = CurPacket.ReadChars(imageNameLength);
                        string script = CurPacket.ReadString().Text;
                        script = script.Replace('§', '\n');
                        Information.serverWindow.OpenWeapon(weaponName, script);
                        break;

                    case PacketOut.NC_CLASSGET:
                        int classNameLegth = CurPacket.ReadGUByte1();
                        string className = CurPacket.ReadChars(classNameLegth);
                        string classScript = CurPacket.ReadString().Untokenize().Text;
                        Information.serverWindow.OpenClass(className, classScript);
                        break;

                    case PacketOut.NC_CLASSADD:
                        string ncadd_className = CurPacket.ReadString().Text;
                        Information.serverWindow.serverWindowClassList.AddClass(ncadd_className);
                        break;

                    case PacketOut.NC_NPCADD:
                        int dbId = CurPacket.ReadGByte3();
                        CurPacket.ReadChars(1);
                        int dbNameLen = CurPacket.ReadGUByte1();
                        string dbName = CurPacket.ReadChars(dbNameLen);
                        CurPacket.ReadChars(1);
                        int dbTypeLen = CurPacket.ReadGUByte1();
                        string dbType = CurPacket.ReadChars(dbTypeLen);
                        CurPacket.ReadChars(1);
                        int dbLevelLen = CurPacket.ReadGUByte1();
                        string dbLevel = CurPacket.ReadChars(dbLevelLen);

                        Information.serverWindow.serverWindowNPCList.AddNPC(dbId.ToString(), dbName, dbType, dbLevel);
                        break;

                    case PacketOut.NC_NPCSCRIPT:
                        int dbGetId = CurPacket.ReadGByte3();
                        string dbGetScript = CurPacket.ReadString().Untokenize().Text;

                        Information.serverWindow.OpenDB(dbGetId, dbGetScript);
                        break;
                }
            }
        }
    }
}
