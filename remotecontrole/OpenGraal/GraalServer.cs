﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenGraal;
using OpenGraal.Core;
using OpenGraal.GraalIM;
using OpenGraal.Common.Players;
using OpenGraal.Common.Connections.Client;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.CompilerServices;
using RemoteControlE;

namespace OpenGraal.GraalIM.Connections
{
    public class GraalServer : OpenGraal.Common.Connections.Client.GraalServer
    {

        /// <summary>
        /// Member Variables
        /// </summary>
        private static Framework Server;
        //private static PMWindowList PMWindowManager = PMWindowList.GetInstance();
        //protected Abstraction form;
        public string Nickname;
        List<string> dirList = new List<string>();
        public GraalPlayerList PlayerManager = new GraalPlayerList();
        ServerWindowComments commentsWindow;

        public event MyEventHandler OnGSStringChange;

        public void RunServer()
        {
            Server = Framework.GetInstance();
            //this.form = Abstraction.GetInstance();
            this.Init();
            this.Setup();
        }

        public void SendLogin(String Account, String Password, String Nickname)
        {
            this.Codec.Reset(Encrypt.Generation.GEN5);
            //Set the nickname
            this.Nickname = Nickname;
            //this.form = Abstraction.GetInstance();
            string versionStr = "G3D0208A";
            versionStr = "GSERV025";

            this.SendLogin(Information.Account, Information.Password, Information.Nickname, versionStr, true);
            this.ReceiveData();
        }

        public override void AddPlayer(short playerId, string playerAccount, System.Collections.Generic.Dictionary<int,dynamic> props)
        {
            foreach (KeyValuePair<int,dynamic> prop in props)
            {
                //Console.WriteLine(prop.Key + "=" + prop.Value);
                
            }

            
            if (playerAccount.StartsWith("irc:")) return;

            string nickname = props[0];
            string level = props[20];
            Information.serverWindow.serverWindowPlayers.AddPlayer(nickname, playerAccount, level);

        }

        public void PlayerAdded(GraalPlayer player)
        {
            MessageBox.Show(player.Nickname);
        }

        public void QueryWeaponList()
        {
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "GraalEngine,lister,weaponlist,\"\"");
        }

        public override void ReceivedServerFlags(int serverFlagsTotal, string serverFlagsString)
        {
            //this.form.WriteText(serverFlagsTotal.ToString() + serverFlagsString);
        }

        public override void ReceivedServerOptions(string serverOptions)
        {
            //MessageBox.Show(serverOptions);
            Information.serverWindow.serverWindowServerOptions.SetText(serverOptions);
        }

        public override void ReceivedFolderConfig(string folderConfig)
        {
            //this.form.WriteText(folderConfig);
        }

        //public override void ReceivedPM(string PlayerId, CString Message)
        //{
        //    GraalPlayer PMPlayer = this.PlayerManager.FindPlayer(PlayerId);
        //    //this.ReceivedPM(PMPlayer, Message);
        //    MessageBox.Show(PlayerId + ": " + Message.Text);
        //}

        public override void ReceivedPM(short PlayerId, CString Message)
        {
            GraalPlayer PMPlayer = this.PlayerManager.FindPlayer(PlayerId);
            if (PMPlayer != null)
                //this.ReceivedPM(PMPlayer, Message);
                MessageBox.Show(PlayerId + ": " + Message.Text);
            else
                return;
        }

 
        public void ReceivedPM(GraalPlayer Player, CString Message)
        {

            if (Player != null)
            {

                if (Message != null)
                {
                    //Console.WriteLine(String.Format("PM from {0}: {1}", Player.Account, Message));
                }
            }
            else
            {
                //Console.WriteLine("Something went wrong: Player is null");
            }
        }

        //public override void ReceivedToall(short PlayerId, CString Message)
        //{
        //    GraalPlayer PMPlayer = this.PlayerManager.FindPlayer(PlayerId);
        //    if (PMPlayer != null)
        //        this.ReceivedToall(PMPlayer, Message);
        //    else
        //        this.form.WriteText("Something went wrong. PMPlayer is null.");
        //}

        public /*override*/ void ReceivedFileBrowserMessage(CString message)
        {
            //Gtk.Application.Invoke(delegate
            //{
            //    RCFileBrowser.GetInstance().SetMessage(message);
            //    RCFileBrowser.GetInstance().ShowAll();
            //});

            MessageBox.Show(message.Text);
            Information.serverWindow.serverWindowFileBrowser.ToConsole(message.Text);
        }

        public /*override*/ void ReceivedFileBrowserDirList(string[] dirList)
        {
            //Gtk.Application.Invoke(delegate
            //{
            //    RCFileBrowser.GetInstance().SetDirList(dirList);
            //    RCFileBrowser.GetInstance().ShowAll();
            //});
            //Console.WriteLine("RECEIVED LIST");
            foreach (string dir in dirList)
            {
                //Console.WriteLine("Dir: " + dir);

                Server.SendRC_CHAT(dir);
            }


        }

        public void ReceivedToall(GraalPlayer Player, CString Message)
        {
            //this.form = Abstraction.GetInstance();

            //ToallsWindow PM = ToallsWindow.GetInstance();

            //if (PM != null)
            //{
            //    PM.SetMessage(Player, Message);
            //}
        }

        public override void WriteText(string text)
        {
            //this.form.WriteText(text);
            //Console.WriteLine(text);

            if (text.Contains(":") == false) return;

            string prefix = text.Substring(0, text.IndexOf(":"));
            string content = text.Substring(text.IndexOf(":") + 2);

            switch (prefix)
            {
                case "[RC_FILEBROWSER_DIRLIST]":
                    string[] dirArray = content.Split(',');
                    foreach (string dir in dirArray)
                    {
                        string dirName = dir.Substring(1, dir.Length - 2);
                        Information.serverWindow.serverWindowFileBrowser.AddDirectory(dirName.Substring(3, dirName.Length - 5));

                        dirList.Add(dirName);
                    }

                    Information.serverWindow.serverWindowFileBrowser.FinishedLoadingDirectories();

                    Server.SendRC_CHAT(String.Format("Successfully loaded {0} directories", dirList.Count.ToString()));

                    DownloadFiles();
                    break;

                case "[FILE]":
                    //Console.WriteLine("Got content: " + content);
                    break;

                default:
                    //Console.WriteLine(text);
                    break;
            }
        }

        public void DownloadFiles()
        {
        }

        protected override void HandleData(CString Packet)
        {
            int num = 0;
            uint pCount = 0;
            while (Packet.BytesLeft > 0)
            {
                CString str;
                short num9;
                CString str11;
                int num10;
                List<string> list;
                string str13;
                if (num == 100)
                {
                    str = Packet.ReadChars2(pCount);
                }
                else
                {
                    str = Packet.ReadString('\n');
                }
                int num3 = str.ReadGUByte1();
                switch (((PacketIn)num3))
                {
                    case PacketIn.LEVELLINK:
                    case PacketIn.LEVELSIGN:
                    case PacketIn.LEVELNAME:
                    case PacketIn.OTHERPLPROPS:
                    case PacketIn.PLAYERPROPS:
                    case PacketIn.SHOWIMG:
                    case PacketIn.NPCWEAPONDEL:
                    case PacketIn.NEWWORLDTIME:
                    case PacketIn.UNKNOWN60:
                    case PacketIn.BOARDPACKET:
                    case PacketIn.SETACTIVELEVEL:
                        goto Label_04DC;

                    case PacketIn.NPCPROPS:
                        int num4 = str.ReadGByte3();
                        goto Label_04DC;

                    case PacketIn.TOALL:
                        num9 = str.ReadGByte2();
                        str11 = str.ReadString();
                        this.ReceivedToall(num9, str11);
                        goto Label_04DC;

                    case PacketIn.DISCMESSAGE:
                        string text = str.ReadString().Text;
                        this.Disconnected(text);
                        goto Label_04DC;

                    case PacketIn.RC_FILEBROWSER_MESSAGE:
                        string rctext = str.ReadString().Text;
                        Information.serverWindow.serverWindowFileBrowser.ToConsole(rctext);

                        goto Label_04DC;

                    case PacketIn.SIGNATURE:
                        Console.WriteLine(str);
                        this.ReceivedSignature();
                        goto Label_04DC;

                    case PacketIn.FLAGSET:
                        string str6 = str.ReadString('=').Text;
                        string str7 = str.ReadString().Text;
                        goto Label_04DC;

                    case PacketIn.NPCWEAPONADD:
                        string str8 = str.ReadChars(str.ReadGUByte1());
                        string str9 = str.ReadChars(str.ReadGUByte1());
                        string str10 = str.ReadString().Text;
                        goto Label_04DC;

                    case PacketIn.PRIVATEMESSAGE:
                        num9 = str.ReadGByte2();
                        str11 = str.ReadString();
                        this.ReceivedPM(num9, str11);
                        goto Label_04DC;

                    case PacketIn.LEVELMODTIME:
                        uint num5 = str.ReadGUByte5();
                        goto Label_04DC;

                    case PacketIn.ADDPLAYER:
                        short playerId = str.ReadGByte2();
                        string playerAccount = str.ReadChars(str.ReadGUByte1());
                        Dictionary<int, object> props = GraalPlayer.ParseProps(str);
                        this.AddPlayer(playerId, playerAccount, props);
                        goto Label_04DC;

                    case PacketIn.DELPLAYER:
                        short num8 = str.ReadGByte2();
                        goto Label_04DC;

                    case PacketIn.RC_PLAYERCOMMENTSGET:
                        string account = str.ReadChars(str.ReadGUByte1());
                        string content = str.ReadString().Untokenize().Text;
                        this.ReceivedComments(account, content);
                        goto Label_04DC;

                    case PacketIn.RC_SERVERFLAGSGET:
                        num10 = str.ReadGByte2();
                        list = new List<string>();
                        str13 = "";
                        goto Label_0354;

                    case PacketIn.RC_FILEBROWSER_DIR:
                        List<string> dirFiles = new List<string>();

                        int dirNameLen = str.ReadGUByte1();
                        string dirName = str.ReadChars(dirNameLen);
                        
                        //Console.WriteLine("Dir: " + dirName);
                        while (str.BytesLeft > 0)
                        {
                            str.ReadChars(1);
                            var dirFileName = str.ReadChars(str.ReadGUByte2());
                            var dirFileName2 = str.ReadChars(str.ReadGUByte1());
                            //Console.WriteLine(dirFileName);
                            //Console.WriteLine(dirFileName2);
                        }

                        num10 = str.ReadGByte2();
                        str13 = "";
                        goto Label_04DC;

                    case PacketIn.RC_CHAT:
                        this.ReceivedRCChat(str.ReadString().Text);
                        goto Label_04DC;

                    case PacketIn.RC_SERVEROPTIONSGET:
                        string serverOptions = str.ReadString().Untokenize().ToString();
                        this.ReceivedServerOptions(serverOptions);
                        goto Label_04DC;

                    case PacketIn.RC_PLAYERPROPSGET:
                        short getPlayerId = str.ReadGByte2();
                        string getPlayerAccount = str.ReadChars(str.ReadGUByte1());
                        str.ReadChars(str.ReadGUByte1());

                        //Dictionary<int, object> getProps = GraalPlayer.ParseProps(str);

                        Information.serverWindow.OpenPlayer();
                        goto Label_04DC;

                    case PacketIn.RC_FOLDERCONFIGGET:
                        {
                            string folderConfig = str.ReadString().Untokenize().ToString();
                            this.ReceivedFolderConfig(folderConfig);
                            goto Label_04DC;
                        }
                    case PacketIn.NPCSERVERADDR:
                        {
                            str.readGUShort();
                            CString ip = str.ReadString(',');
                            CString port = str.ReadString();
                            this.ReceivedNpcServerAddress(ip, port);
                            goto Label_04DC;
                        }
                    case PacketIn.SERVERTEXT:
                        {
                            str11 = str.ReadString();
                            List<string> textArray = new List<string>(CString.untokenize(str11.Text).Split(new char[] { '\n' }));
                            this.ReceivedServertext(textArray);
                            goto Label_04DC;
                        }
                    case PacketIn.RAWDATA:
                        pCount = str.ReadGUByte3();
                        goto Label_04DC;

                    case PacketIn.NPCDEL2:
                        {
                            string str4 = str.ReadChars(str.ReadGUByte1());
                            int num6 = str.ReadGByte3();
                            this.DeleteLevelNpc(str4, num6);
                            goto Label_04DC;
                        }
                    default:
                        //Console.WriteLine("PacketId: " + num3.ToString());
                        try
                        {
                            this.WriteText("[" + Enum.GetName(typeof(PacketIn), num3).ToString() + "]: " + str.ReadString().Text);
                        }
                        catch (IndexOutOfRangeException)
                        {
                        }
                        catch (NullReferenceException)
                        {
                        }
                        goto Label_04DC;
                }
           Label_033E:
                list.Add(str.ReadChars(str.ReadGUByte1()));
            Label_0354:
                if (str.BytesLeft > 0)
                {
                    goto Label_033E;
                }
                list.Sort();
                foreach (string str14 in list)
                {
                    str13 = str13 + str14 + "\n";
                }
                this.ReceivedServerFlags(num10, str13);
            Label_04DC:
                num = num3;
            }

        
        }

        public void ReceivedComments(string account, string content)
        {
            Information.serverWindow.OpenComments(account, content);
        }

        public override void ReceivedRCChat(string text)
        {
            Information.serverWindow.serverWindowChat.UpdateRCChat(text);
        }

        public override void ReceivedNpcServerAddress(CString ip, CString port)
        {
            Server.InitNcConnection(ip, port);
        }

        public override void ReceivedSignature()
        {
            //this.form.WriteText("Connected!");
            //Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.PLAYERPROPS + (byte)GraalServer.PLPROPS.MAXPOWER + (byte)0);
            //Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.PLAYERPROPS + (byte)GraalServer.PLPROPS.NICKNAME + (byte)this.Nickname.Length + this.Nickname);


            /*
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "GraalEngine,pmguilds,\"\"");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "GraalEngine,pmservers,\"all\"");
            //Server.SendGSPacket(new CString() + (byte)PacketOut.REQUESTTEXT + "GraalEngine,pmserverplayers,\"U Classic iPhone\"\n");

            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.SENDTEXT + "GraalEngine,lister,options,globalpms=true,buddytracking=true,showbuddies=true");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.SENDTEXT + "GraalEngine,lister,verifybuddies,1,1964252486");

            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "GraalEngine,lister,addbuddy,\"unixmad\"");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "GraalEngine,lister,addbuddy,\"stefan\"");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "GraalEngine,lister,addbuddy,\"tig\"");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "GraalEngine,lister,addbuddy,\"skyld\"");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "GraalEngine,lister,addbuddy,\"xor\"");
            //Server.SendGSPacket(new CString() + (byte)PacketOut.REQUESTTEXT + "-ShopGlobal,lister,getglobalitems\n");

            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "-Serverlist,lister,list,all");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "-Serverlist,lister,upgradeinfo\n");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "-Serverlist,lister,subscriptions\n");
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.REQUESTTEXT + "-Serverlist,lister,getlockcomputer");
            */
            // Logging into IRC. 
            //Server.SendGSPacket(new CString() + (byte)154 + "GraalEngine,irc,login,-");
            //Server.SendGSPacket(new CString() + (byte)154 + "-Serverlist_Chat,irc,privmsg,IRCBot,!geteventbots");

            //Console.WriteLine("Got signature!");

            Thread.Sleep(500);
            this.SendPacket(new CString() + (byte)94 + " \"location");
            this.SendPacket(new CString() + (byte)154 + "GraalEngine,irc,login,-", true);

            Information.Framework.SendRC_CHAT("Logged in with RCE (Version: " + Data.APP_VERSION + ")");

            // Change name
            Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.PLAYERPROPS + (byte)GraalServer.PLPROPS.NICKNAME + (byte)this.Nickname.Length + this.Nickname);
            // Open file browser
            //Server.SendGSPacket(new CString() + (byte)0x59);

            //Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.RC_FILEBROWSER_CD + "\"logs/\"");
            //PacketCDDir("logs");
            //Thread.Sleep(1000);
            //PacketDownloadFile("logs/login.txt");

            //Server.SendRC_CHAT(String.Format("Successfully logged in as {0}", Information.Account));
            //Server.SendRC_CHAT(String.Format("Getting directory list..."));

            //Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.RC_FILEBROWSER_CD + "\"logs/\""); ;
            //this.SendPacket(new CString() + (byte)PacketOut.NC_WEAPONLISTGET, true);
        }

        public /*override*/ void ReceivedUnknown194()
        {
            //Server.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.PLAYERPROPS + (byte)GraalServer.PLPROPS.BODYIMG + (byte)1);
            //Server.SendGSPacket(new CString() + (byte)94 + " \"location");
        }

        public void ConnectedToNC()
        {
            MessageBox.Show("Connected!");
        }

        public override void ReceivedServertext(List<string> serverText)
        {
            string messageData = "";
            for (var i = 0; i < serverText.Count; i++)
            {
                messageData += serverText[i] + ",";
            }

            //Console.WriteLine(">> (Server Text): " + messageData);


            if (serverText[1].Trim() == "lister")
            {
                if (serverText[1].Trim() == "lister")
                {
                }
            }
            else if (serverText[1].Trim() == "irc")
            {

                if (serverText[1].Trim() == "irc")
                {

                    switch (serverText[2].Trim())
                    {
                        case "join":
                            {
                                string channel = serverText[3].Trim();
                                Information.serverWindow.serverWindowChat.AddIRCTab(channel);

                                break;
                            }

                        case "addchanneluser"://[SERVERTEXT]: -Serverlist_Chat irc addchanneluser #graal CrypticMyst login1 Gos_Pira 
                            {
                                //this.form.IRC_AddChannelUser(serverText[3].Trim(), serverText[4].Trim());
                                break;
                            }
                        case "deletechanneluser"://[SERVERTEXT]: -Serverlist_Chat irc delchanneluser #graal CrypticMyst
                            {
                                //this.form.IRC_AddChannelUser(serverText[3].Trim(), serverText[4].Trim(), true);
                                break;
                            }
                        case "privmsg":
                            {
                                string message = serverText[5].Trim();
                                if (serverText[4].Trim().StartsWith("#"))
                                    //this.form.IRC_Privmsg(serverText[3].Trim(), serverText[4].Trim(), message);
                                    return;
                                else
                                {

                                    this.ReceivedPM(serverText[3].Trim(), new CString(message));
                                }
                                break;
                            }

                        default:
                            {

                                break;
                            }
                    }

                }
            }
            else if (serverText[0].Trim() == "-Serverlist")
            {
                if (serverText[1].Trim() == "lister")
                {
                    #region SimpleServerList
                    if (serverText[2].Trim() == "subscriptions2")
                    {
                        string subscription = "Trial";
                        // Console.WriteLine(test2.Count.ToString());

                        if (serverText.Count > 3)
                        {
                            //Console.WriteLine(test2.Count.ToString());
                            string server2 = CString.untokenize(serverText[3].Trim());
                            string[] test3 = server2.Split('\n');
                            subscription = test3[1].Trim();
                        }

                        //this.form.SetSubscriptionText("Subscription: " + subscription);
                    }
                    else if (serverText[2].Trim() == "lockcomputer")
                    {
                        int locked = 0;
                        int.TryParse(serverText[3].Trim(), out locked);

                        //this.form.SetLockedByComputerText("Locked by PC-ID: " + ((locked == 1) ? "Yes" : "No"));
                    }
                    else if (serverText[2].Trim() == "simpleserverlist" || serverText[2].Trim() == "serverlist")
                    {
                        List<string> servers = serverText;
                        servers.RemoveRange(0, 3);
                        //Console.WriteLine("SERVERS(" + servers.Count + "):");
                        //Console.WriteLine("------------");
                        foreach (string server in servers)
                        {
                            //CString server2 = new CString();
                            //server2.Write(server);
                            if (server != "")
                            {
                                string server2 = CString.untokenize(server);
                                string[] test3 = server2.Split('\n');
                                //this.form.Write_Text(server2 + "\r\n");
                                var servername = "";
                                var servertype = "";
                                if (test3[1].Trim().Substring(0, 2) == "P " || test3[1].Trim().Substring(0, 2) == "H " || test3[1].Trim().Substring(0, 2) == "3 " || test3[1].Trim().Substring(0, 2) == "U ")
                                {
                                    servertype = test3[1].Trim().Substring(0, 2);
                                    servername = test3[1].Trim().Substring(2, test3[1].Trim().Length - 2);
                                    if (servertype == "P ")
                                        servertype = "Gold";
                                    if (servertype == "H ")
                                        servertype = "Hosted";
                                    if (servertype == "3 ")
                                        servertype = "3D";
                                    if (servertype == "U ")
                                        servertype = "Hidden";
                                }
                                else
                                {
                                    servertype = "Classic";
                                    servername = test3[1].Trim();
                                }

                                var serverid = test3[0].Trim();
                                var serverpc = test3[2].Trim();


                                //this.form.WriteText(" * Id: " + serverid + " Type: " + servertype + " Name: " + servername + " Players: " + serverpc + "");
                            }

                        }
                        //this.form.WriteText("");
                    }
                    else
                    { }

                    #endregion
                }
            }
            else
            { } //Console.WriteLine("[SERVERTEXT]: " + TheMessage2.Replace("\r\n", ",") + "");
        }

        public delegate void MyEventHandler(object source, MyEventArgs e);

        public class MyEventArgs : EventArgs
        {
            private string EventInfo;
            public MyEventArgs(string Text)
            {
                EventInfo = Text;
            }
            public string GetInfo()
            {
                return EventInfo;
            }
        }
    }
}
