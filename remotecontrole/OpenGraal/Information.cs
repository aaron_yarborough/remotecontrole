﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RemoteControlE;
using OpenGraal.GraalIM.Connections;
using OpenGraal.NpcServer;

namespace OpenGraal.GraalIM
{
    public static class Information
    {
        public static string Account { get; set; }
        public static string Password { get; set; }
        public static string Nickname { get; set; }
        public static string ServerName { get; set; }
        public static string ServerIp { get; set; }
        public static int ServerPort { get; set; }
        public static ServerWindow serverWindow { get; set; }
        public static Framework Framework { get; set; }
        public static GraalServer GraalServer { get; set; }
        public static ServerListWindow ServerListWindow { get; set; }
        public static ServerWindowComments CommentsWindow { get; set; }
        public static NCConnection NcConnection { get; set; }
    }
}
