﻿namespace OpenGraal.GraalIM
{
    using OpenGraal.Core;
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public class ListServerConnection : CSocket
    {
        public string errormsg = "";
        public string n_servers = "";
        public string Nickname;
        public TServerList serverList;

        public string GetMD5Hash(string input)
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            byte[] bytes = Encoding.UTF8.GetBytes(input);
            bytes = provider.ComputeHash(bytes);
            StringBuilder builder = new StringBuilder();
            foreach (byte num in bytes)
            {
                builder.Append(num.ToString("x2").ToLower());
            }
            return builder.ToString();
        }

        protected override void HandleData(CString Packet)
        {
            int num = 0;
            uint pCount = 0;
            while (Packet.BytesLeft > 0)
            {
                CString str;
                if (num == 100)
                {
                    str = Packet.ReadChars2(pCount);
                }
                else
                {
                    str = Packet.ReadString('\n');
                }
                int num3 = str.ReadGUByte1();
                switch (((PacketIn)num3))
                {
                    case PacketIn.LEVELBOARD:
                        {
                            int num4 = str.ReadGUByte1();
                            this.n_servers = "Number of servers: " + num4.ToString();
                            this.serverList = new TServerList();
                            for (int i = 0; i < num4; i++)
                            {
                                int num6;
                                str.ReadGUByte1();
                                TServer server = this.serverList.AddTServer((short)i);
                                server.setName(new CString(str.ReadChars(str.ReadGUByte1())));
                                server.setLanguage(new CString(str.ReadChars(str.ReadGUByte1())));
                                server.setDescription(new CString(str.ReadChars(str.ReadGUByte1())));
                                server.setUrl(new CString(str.ReadChars(str.ReadGUByte1())));
                                server.setVersion(new CString(str.ReadChars(str.ReadGUByte1())));
                                server.setPCount(new CString(str.ReadChars(str.ReadGUByte1())));
                                server.setIp(new CString(str.ReadChars(str.ReadGUByte1())));
                                int.TryParse(str.ReadChars(str.ReadGUByte1()), out num6);
                                server.setPort(num6);
                            }
                            break;
                        }
                    case PacketIn.LEVELCHEST:
                        {
                            CString str2 = str.ReadString();
                            this.errormsg = str2.Text.ToString();
                            break;
                        }
                }
                num = num3;
            }
        }

        public void SendLogin(string Account, string Password, string Nickname)
        {
            Console.WriteLine(String.Format("Account: {0}\tPassword: {1}\tNickname: {2}", Account, Password, Nickname));
            this.Nickname = Nickname;
            this.errormsg = "";
            CString str = ((((((new CString("") + 0) + "rc\n!") + ((byte)Account.Length)) + Account) + ((byte)Password.Length)) + Password) + "\n";
            str.ZCompress().PreLength();
            base.Send(str.Buffer);
        }

        public enum NCREQ
        {
            NPCLOG,
            GETWEAPONS,
            GETLEVELS,
            SENDPM,
            SENDTORC,
            WEAPONADD,
            WEAPONDEL,
            PLSETPROPS,
            PLGETWEPS,
            PLSNDPACKET,
            PLADDWEP,
            PLDELWEP,
            LEVELGET,
            NPCPROPSET,
            NPCWARP,
            PLRPGMSG,
            PLSETFLAG,
            PLMSGSIGN,
            PLSETSTATUS,
            NPCMOVE
        }

        public enum PacketIn
        {
            ADDPLAYER = 0x37,
            ARROWADD = 0x13,
            BADDYHURT = 0x1b,
            BADDYPROPS = 2,
            BIGMAP = 0xab,
            BOARDMODIFY = 7,
            BOARDPACKET = 0x65,
            BOMBADD = 11,
            BOMBDEL = 12,
            DEFAULTWEAPON = 0x2b,
            DELPLAYER = 0x38,
            DISCMESSAGE = 0x10,
            EXPLOSION = 0x24,
            FILE = 0x66,
            FILESENDFAILED = 30,
            FILEUPTODATE = 0x2d,
            FIRESPY = 20,
            FLAGDEL = 0x1f,
            FLAGSET = 0x1c,
            FrameworkADDR = 0x4f,
            FREEZEPLAYER2 = 0x9a,
            FULLSTOP = 0xb0,
            FULLSTOP2 = 0xb1,
            GHOSTICON = 0xae,
            GHOSTMODE = 170,
            GHOSTTEXT = 0xad,
            HASFramework = 0x2c,
            HIDENPCS = 0x97,
            HITOBJECTS = 0x2e,
            HORSEADD = 0x11,
            HORSEDEL = 0x12,
            HURTPLAYER = 40,
            ISLEADER = 10,
            ITEMADD = 0x16,
            ITEMDEL = 0x17,
            LARGEFILEEND = 0x45,
            LARGEFILESIZE = 0x54,
            LARGEFILESTART = 0x44,
            LEVELBOARD = 0,
            LEVELCHEST = 4,
            LEVELLINK = 1,
            LEVELMODTIME = 0x27,
            LEVELNAME = 6,
            LEVELSIGN = 5,
            LISTPROCESSES = 0xb6,
            MINIMAP = 0xac,
            MOVE = 0xa5,
            MOVE2 = 0xbd,
            NC_CLASSADD = 0xa3,
            NC_CLASSDELETE = 0xbc,
            NC_CLASSGET = 0xa2,
            NC_CONTROL = 0x4e,
            NC_LEVELDUMP = 0xa4,
            NC_LEVELLIST = 80,
            NC_NPCADD = 0x9e,
            NC_NPCATTRIBUTES = 0x9d,
            NC_NPCDELETE = 0x9f,
            NC_NPCFLAGS = 0xa1,
            NC_NPCSCRIPT = 160,
            NC_WEAPONGET = 0xc0,
            NC_WEAPONLISTGET = 0xa7,
            NEWWORLDTIME = 0x2a,
            NPCACTION = 0x1a,
            NPCBYTECODE = 0x83,
            NPCDEL = 0x1d,
            NPCDEL2 = 150,
            NPCMOVED = 0x18,
            NPCPROPS = 3,
            NPCWEAPONADD = 0x21,
            NPCWEAPONDEL = 0x22,
            NPCWEAPONSCRIPT = 140,
            OTHERPLPROPS = 8,
            PLAYERPROPS = 9,
            PLAYERWARP = 14,
            PLAYERWARP2 = 0x31,
            PRIVATEMESSAGE = 0x25,
            PROFILE = 0x4b,
            PUSHAWAY = 0x26,
            RAWDATA = 100,
            RC_ACCOUNTADD = 50,
            RC_ACCOUNTCHANGE = 0x3a,
            RC_ACCOUNTDEL = 0x35,
            RC_ACCOUNTGET = 0x49,
            RC_ACCOUNTLISTGET = 70,
            RC_ACCOUNTNAME = 0x34,
            RC_ACCOUNTPROPS = 0x36,
            RC_ACCOUNTPROPSGET = 0x39,
            RC_ACCOUNTSTATUS = 0x33,
            RC_ADMINMESSAGE = 0x23,
            RC_CHAT = 0x4a,
            RC_FILEBROWSER_DIR = 0x42,
            RC_FILEBROWSER_DIRLIST = 0x41,
            RC_FILEBROWSER_MESSAGE = 0x43,
            RC_FOLDERCONFIGGET = 0x4d,
            RC_PLAYERBANGET = 0x40,
            RC_PLAYERCOMMENTSGET = 0x3f,
            RC_PLAYERPROPS = 0x47,
            RC_PLAYERPROPSCHANGE = 0x3b,
            RC_PLAYERPROPSGET = 0x48,
            RC_PLAYERRIGHTSGET = 0x3e,
            RC_SERVERFLAGSGET = 0x3d,
            RC_SERVEROPTIONSGET = 0x4c,
            RPGWINDOW = 0xb3,
            SAY2 = 0x99,
            SERVERTEXT = 0x52,
            SERVERWARP = 0xb2,
            SETACTIVELEVEL = 0x9c,
            SHOOT = 0xaf,
            SHOWIMG = 0x20,
            SIGNATURE = 0x19,
            STAFFGUILDS = 0x2f,
            STARTMESSAGE = 0x29,
            STATUSLIST = 180,
            THROWCARRIED = 0x15,
            TOALL = 13,
            TRIGGERACTION = 0x30,
            UNFREEZEPLAYER = 0x9b,
            UNKNOWN134 = 0x86,
            UNKNOWN168 = 0xa8,
            UNKNOWN190 = 190,
            UNKNOWN194 = 0xc2,
            UNKNOWN195 = 0xc3,
            UNKNOWN197 = 0xc5,
            UNKNOWN60 = 60,
            UNKNOWN81 = 0x51,
            UNKNOWN83 = 0x53,
            WARPFAILED = 15
        }

        public enum PacketOut
        {
            ADJACENTLEVEL = 0x23,
            ARROWADD = 9,
            BADDYADD = 0x11,
            BADDYHURT = 0x10,
            BADDYPROPS = 15,
            BOARDMODIFY = 1,
            BOMBADD = 4,
            BOMBDEL = 5,
            CLAIMPKER = 14,
            EXPLOSION = 0x1b,
            FIRESPY = 10,
            FLAGDEL = 0x13,
            FLAGSET = 0x12,
            FrameworkQUERY = 0x5e,
            HITOBJECTS = 0x24,
            HORSEADD = 7,
            HORSEDEL = 8,
            HURTPLAYER = 0x1a,
            ITEMADD = 12,
            ITEMDEL = 13,
            ITEMTAKE = 0x20,
            LANGUAGE = 0x25,
            LEVELWARP = 0,
            LEVELWARPMOD = 30,
            MAPINFO = 0x27,
            NC_CLASSADD = 0x71,
            NC_CLASSDELETE = 0x77,
            NC_CLASSEDIT = 0x70,
            NC_LEVELLISTGET = 150,
            NC_LEVELLISTSET = 0x97,
            NC_LOCALNPCSGET = 0x72,
            NC_NPCADD = 0x6f,
            NC_NPCDELETE = 0x68,
            NC_NPCFLAGSGET = 0x6c,
            NC_NPCFLAGSSET = 110,
            NC_NPCGET = 0x67,
            NC_NPCRESET = 0x69,
            NC_NPCSCRIPTGET = 0x6a,
            NC_NPCSCRIPTSET = 0x6d,
            NC_NPCWARP = 0x6b,
            NC_WEAPONADD = 0x75,
            NC_WEAPONDELETE = 0x76,
            NC_WEAPONGET = 0x74,
            NC_WEAPONLISTGET = 0x73,
            NPCDEL = 0x16,
            NPCPROPS = 3,
            NPCWEAPONDEL = 0x1d,
            OPENCHEST = 20,
            PACKETCOUNT = 0x1f,
            PLAYERPROPS = 2,
            PRIVATEMESSAGE = 0x1c,
            PROCESSLIST = 0x2c,
            PROFILEGET = 80,
            PROFILESET = 0x51,
            PUTNPC = 0x15,
            RAWDATA = 50,
            RC_ACCOUNTADD = 70,
            RC_ACCOUNTDEL = 0x47,
            RC_ACCOUNTGET = 0x4d,
            RC_ACCOUNTLISTGET = 0x48,
            RC_ACCOUNTSET = 0x4e,
            RC_ADMINMESSAGE = 0x3f,
            RC_APINCREMENTSET = 0x39,
            RC_APPLYREASON = 0x43,
            RC_BADDYRESPAWNSET = 0x3a,
            RC_CHAT = 0x4f,
            RC_DISCONNECTPLAYER = 0x3d,
            RC_DISCONNECTRC = 0x42,
            RC_FILEBROWSER_CD = 90,
            RC_FILEBROWSER_DELETE = 0x61,
            RC_FILEBROWSER_DOWN = 0x5c,
            RC_FILEBROWSER_END = 0x5b,
            RC_FILEBROWSER_MOVE = 0x60,
            RC_FILEBROWSER_RENAME = 0x62,
            RC_FILEBROWSER_START = 0x59,
            RC_FILEBROWSER_UP = 0x5d,
            RC_FOLDERCONFIGGET = 0x35,
            RC_FOLDERCONFIGSET = 0x36,
            RC_FOLDERDELETE = 160,
            RC_HORSELIFESET = 0x38,
            RC_LARGEFILEEND = 0x9c,
            RC_LARGEFILESTART = 0x9b,
            RC_LISTRCS = 0x41,
            RC_PLAYERBANGET = 0x57,
            RC_PLAYERBANSET = 0x58,
            RC_PLAYERCOMMENTSGET = 0x55,
            RC_PLAYERCOMMENTSSET = 0x56,
            RC_PLAYERPROPSGET = 0x3b,
            RC_PLAYERPROPSGET2 = 0x49,
            RC_PLAYERPROPSGET3 = 0x4a,
            RC_PLAYERPROPSRESET = 0x4b,
            RC_PLAYERPROPSSET = 60,
            RC_PLAYERPROPSSET2 = 0x4c,
            RC_PLAYERRIGHTSGET = 0x53,
            RC_PLAYERRIGHTSSET = 0x54,
            RC_PRIVADMINMESSAGE = 0x40,
            RC_RESPAWNSET = 0x37,
            RC_SERVERFLAGSGET = 0x44,
            RC_SERVERFLAGSSET = 0x45,
            RC_SERVEROPTIONSGET = 0x33,
            RC_SERVEROPTIONSSET = 0x34,
            RC_UPDATELEVELS = 0x3e,
            RC_WARPPLAYER = 0x52,
            REQUESTTEXT = 0x98,
            SENDTEXT = 0x9a,
            SERVERWARP = 0x29,
            SHOOT = 40,
            SHOWIMG = 0x18,
            THROWCARRIED = 11,
            TOALL = 6,
            TRIGGERACTION = 0x26,
            UNKNOWN157 = 0x9d,
            UNKNOWN161 = 0xa1,
            UNKNOWN25 = 0x19,
            UNKNOWN46 = 0x2e,
            UNKNOWN47 = 0x2f,
            UPDATEFILE = 0x22,
            UPDATESCRIPT = 0x9e,
            WANTFILE = 0x17,
            WEAPONADD = 0x21
        }

        public enum PLPROPS
        {
            NICKNAME,
            MAXPOWER,
            CURPOWER,
            RUPEESCOUNT,
            ARROWSCOUNT,
            BOMBSCOUNT,
            GLOVEPOWER,
            BOMBPOWER,
            SWORDPOWER,
            SHIELDPOWER,
            GANI,
            HEADGIF,
            CURCHAT,
            COLORS,
            ID,
            X,
            Y,
            SPRITE,
            STATUS,
            CARRYSPRITE,
            CURLEVEL,
            HORSEGIF,
            HORSEBUSHES,
            EFFECTCOLORS,
            CARRYNPC,
            APCOUNTER,
            MAGICPOINTS,
            KILLSCOUNT,
            DEATHSCOUNT,
            ONLINESECS,
            IPADDR,
            UDPPORT,
            ALIGNMENT,
            ADDITFLAGS,
            ACCOUNTNAME,
            BODYIMG,
            RATING,
            GATTRIB1,
            GATTRIB2,
            GATTRIB3,
            GATTRIB4,
            GATTRIB5,
            ATTACHNPC,
            GMAPLEVELX,
            GMAPLEVELY,
            Z,
            GATTRIB6,
            GATTRIB7,
            GATTRIB8,
            GATTRIB9,
            JOINLEAVELVL,
            PCONNECTED,
            PLANGUAGE,
            PSTATUSMSG,
            GATTRIB10,
            GATTRIB11,
            GATTRIB12,
            GATTRIB13,
            GATTRIB14,
            GATTRIB15,
            GATTRIB16,
            GATTRIB17,
            GATTRIB18,
            GATTRIB19,
            GATTRIB20,
            GATTRIB21,
            GATTRIB22,
            GATTRIB23,
            GATTRIB24,
            GATTRIB25,
            GATTRIB26,
            GATTRIB27,
            GATTRIB28,
            GATTRIB29,
            GATTRIB30,
            OSTYPE,
            TEXTCODEPAGE,
            UNKNOWN77,
            X2,
            Y2,
            Z2,
            UNKNOWN81,
            COMMUNITYNAME
        }
    }
}

