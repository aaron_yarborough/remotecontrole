﻿using OpenGraal.GraalIM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteControlE
{
    public partial class ServerListWindow : Form
    {
        public ServerListWindow(TServerList serverList)
        {
            InitializeComponent();

            Information.ServerListWindow = this;

            this.FormClosed += ServerListWindow_FormClosed;
            this.buttonConnect.Click += buttonConnect_Click;

            try
            {
                foreach (TServer server in serverList)
                {
                    ListViewItem newItem = new ListViewItem(new string[] { server.getName(), server.getPCount().ToString(), server.getIp() + server.getPort() });
                    newItem.Tag = server.getIp() + "!" + server.getPort() + "!" + server.getName();

                    listViewServers.Items.Add(newItem);
                }

                listViewServers.Sorting = SortOrder.Ascending;
                listViewServers.Sort();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't connect to the listserver!");
                return;
            }

            listViewServers.MouseDoubleClick += listViewServers_MouseDoubleClick;
        }

        void buttonConnect_Click(object sender, EventArgs e)
        {
            ConnectToSelectedServer();
        }

        void ServerListWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void listViewServers_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ConnectToSelectedServer();
        }

        public void ConnectToSelectedServer()
        {
            if (listViewServers.SelectedItems.Count == 0)
                return;

            this.Hide();

            ListViewItem item = listViewServers.SelectedItems[0];

            string ip = item.Tag.ToString().Split('!')[0];
            int port = Convert.ToInt32(item.Tag.ToString().Split('!')[1]);
            string name = item.Tag.ToString().Split('!')[2];

            ServerWindow serverWindow = new ServerWindow();
            Information.serverWindow = serverWindow;
            serverWindow.Show();

            Information.ServerName = name;
            serverWindow.Text = String.Format("RCE - {0}", Information.ServerName);
            ServerHandler.ConnectToServer(ip, port);
        }
    }
}
