﻿namespace RemoteControlE
{
    partial class OptionsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsWindow));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxDisplayClassListView = new System.Windows.Forms.ComboBox();
            this.comboBoxDisplayWeaponListView = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxScriptEditorIndentSize = new System.Windows.Forms.TextBox();
            this.textBoxScriptEditorFontSize = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonClearLogin = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxVisibleByDefault = new System.Windows.Forms.CheckBox();
            this.checkBoxObjBrowserHideGraalFuncs = new System.Windows.Forms.CheckBox();
            this.checkBoxObjBrowserColourCoding = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelRCEPrefixColour = new System.Windows.Forms.Label();
            this.comboBoxRCEPrefixColour = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxRCEFontFamily = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxRCEFontSize = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.labelScriptEditorBackground = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorBackground = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.labelScriptEditorSynComments = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorSynComments = new System.Windows.Forms.ComboBox();
            this.labelScriptEditorSynFunctions = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorSynFunctions = new System.Windows.Forms.ComboBox();
            this.labelScriptEditorSynFunctionsA = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorSynFunctionsA = new System.Windows.Forms.ComboBox();
            this.labelScriptEditorSynFunctionsN = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorSynFunctionsN = new System.Windows.Forms.ComboBox();
            this.labelScriptEditorSynRegular = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorSynRegular = new System.Windows.Forms.ComboBox();
            this.labelScriptEditorSynStrings = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorSynStrings = new System.Windows.Forms.ComboBox();
            this.labelScriptEditorSynIntegers = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorSynIntegers = new System.Windows.Forms.ComboBox();
            this.labelScriptEditorSynBools = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.comboBoxScriptEditorSynBools = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxDisplayClassListView);
            this.groupBox1.Controls.Add(this.comboBoxDisplayWeaponListView);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 77);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Display";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Class list view:";
            // 
            // comboBoxDisplayClassListView
            // 
            this.comboBoxDisplayClassListView.FormattingEnabled = true;
            this.comboBoxDisplayClassListView.Items.AddRange(new object[] {
            "Details",
            "List"});
            this.comboBoxDisplayClassListView.Location = new System.Drawing.Point(108, 47);
            this.comboBoxDisplayClassListView.Name = "comboBoxDisplayClassListView";
            this.comboBoxDisplayClassListView.Size = new System.Drawing.Size(117, 21);
            this.comboBoxDisplayClassListView.TabIndex = 2;
            // 
            // comboBoxDisplayWeaponListView
            // 
            this.comboBoxDisplayWeaponListView.FormattingEnabled = true;
            this.comboBoxDisplayWeaponListView.Items.AddRange(new object[] {
            "Details",
            "List"});
            this.comboBoxDisplayWeaponListView.Location = new System.Drawing.Point(108, 20);
            this.comboBoxDisplayWeaponListView.Name = "comboBoxDisplayWeaponListView";
            this.comboBoxDisplayWeaponListView.Size = new System.Drawing.Size(117, 21);
            this.comboBoxDisplayWeaponListView.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Weapon list view: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.labelScriptEditorBackground);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.comboBoxScriptEditorBackground);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBoxScriptEditorIndentSize);
            this.groupBox2.Controls.Add(this.textBoxScriptEditorFontSize);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(259, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 130);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Script Editor";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(90, 73);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(135, 20);
            this.textBox1.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Font family:";
            // 
            // textBoxScriptEditorIndentSize
            // 
            this.textBoxScriptEditorIndentSize.Location = new System.Drawing.Point(90, 47);
            this.textBoxScriptEditorIndentSize.Name = "textBoxScriptEditorIndentSize";
            this.textBoxScriptEditorIndentSize.Size = new System.Drawing.Size(135, 20);
            this.textBoxScriptEditorIndentSize.TabIndex = 7;
            // 
            // textBoxScriptEditorFontSize
            // 
            this.textBoxScriptEditorFontSize.Location = new System.Drawing.Point(90, 21);
            this.textBoxScriptEditorFontSize.Name = "textBoxScriptEditorFontSize";
            this.textBoxScriptEditorFontSize.Size = new System.Drawing.Size(135, 20);
            this.textBoxScriptEditorFontSize.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Indent size:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Font size:";
            // 
            // buttonClearLogin
            // 
            this.buttonClearLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonClearLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearLogin.Location = new System.Drawing.Point(88, 469);
            this.buttonClearLogin.Name = "buttonClearLogin";
            this.buttonClearLogin.Size = new System.Drawing.Size(110, 25);
            this.buttonClearLogin.TabIndex = 7;
            this.buttonClearLogin.Text = "Clear login details";
            this.buttonClearLogin.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(12, 469);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(70, 25);
            this.buttonSave.TabIndex = 8;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxVisibleByDefault);
            this.groupBox3.Controls.Add(this.checkBoxObjBrowserHideGraalFuncs);
            this.groupBox3.Controls.Add(this.checkBoxObjBrowserColourCoding);
            this.groupBox3.Location = new System.Drawing.Point(12, 93);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(238, 96);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Object Browser";
            // 
            // checkBoxVisibleByDefault
            // 
            this.checkBoxVisibleByDefault.AutoSize = true;
            this.checkBoxVisibleByDefault.Location = new System.Drawing.Point(12, 69);
            this.checkBoxVisibleByDefault.Name = "checkBoxVisibleByDefault";
            this.checkBoxVisibleByDefault.Size = new System.Drawing.Size(105, 17);
            this.checkBoxVisibleByDefault.TabIndex = 2;
            this.checkBoxVisibleByDefault.Text = "Visible by default";
            this.checkBoxVisibleByDefault.UseVisualStyleBackColor = true;
            // 
            // checkBoxObjBrowserHideGraalFuncs
            // 
            this.checkBoxObjBrowserHideGraalFuncs.AutoSize = true;
            this.checkBoxObjBrowserHideGraalFuncs.Location = new System.Drawing.Point(12, 46);
            this.checkBoxObjBrowserHideGraalFuncs.Name = "checkBoxObjBrowserHideGraalFuncs";
            this.checkBoxObjBrowserHideGraalFuncs.Size = new System.Drawing.Size(174, 17);
            this.checkBoxObjBrowserHideGraalFuncs.TabIndex = 1;
            this.checkBoxObjBrowserHideGraalFuncs.Text = "Hide Graal\'s function definitions";
            this.checkBoxObjBrowserHideGraalFuncs.UseVisualStyleBackColor = true;
            // 
            // checkBoxObjBrowserColourCoding
            // 
            this.checkBoxObjBrowserColourCoding.AutoSize = true;
            this.checkBoxObjBrowserColourCoding.Location = new System.Drawing.Point(12, 23);
            this.checkBoxObjBrowserColourCoding.Name = "checkBoxObjBrowserColourCoding";
            this.checkBoxObjBrowserColourCoding.Size = new System.Drawing.Size(91, 17);
            this.checkBoxObjBrowserColourCoding.TabIndex = 0;
            this.checkBoxObjBrowserColourCoding.Text = "Colour coding";
            this.checkBoxObjBrowserColourCoding.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(12, 458);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(627, 2);
            this.label7.TabIndex = 9;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelRCEPrefixColour);
            this.groupBox4.Controls.Add(this.comboBoxRCEPrefixColour);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.textBoxRCEFontFamily);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.textBoxRCEFontSize);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Location = new System.Drawing.Point(12, 195);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(238, 110);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "RCE Chat";
            // 
            // labelRCEPrefixColour
            // 
            this.labelRCEPrefixColour.BackColor = System.Drawing.Color.DarkRed;
            this.labelRCEPrefixColour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelRCEPrefixColour.Location = new System.Drawing.Point(203, 73);
            this.labelRCEPrefixColour.Name = "labelRCEPrefixColour";
            this.labelRCEPrefixColour.Size = new System.Drawing.Size(22, 21);
            this.labelRCEPrefixColour.TabIndex = 11;
            // 
            // comboBoxRCEPrefixColour
            // 
            this.comboBoxRCEPrefixColour.FormattingEnabled = true;
            this.comboBoxRCEPrefixColour.Location = new System.Drawing.Point(90, 73);
            this.comboBoxRCEPrefixColour.Name = "comboBoxRCEPrefixColour";
            this.comboBoxRCEPrefixColour.Size = new System.Drawing.Size(107, 21);
            this.comboBoxRCEPrefixColour.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Prefix colour:";
            // 
            // textBoxRCEFontFamily
            // 
            this.textBoxRCEFontFamily.Location = new System.Drawing.Point(90, 47);
            this.textBoxRCEFontFamily.Name = "textBoxRCEFontFamily";
            this.textBoxRCEFontFamily.Size = new System.Drawing.Size(135, 20);
            this.textBoxRCEFontFamily.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Font family:";
            // 
            // textBoxRCEFontSize
            // 
            this.textBoxRCEFontSize.Location = new System.Drawing.Point(90, 21);
            this.textBoxRCEFontSize.Name = "textBoxRCEFontSize";
            this.textBoxRCEFontSize.Size = new System.Drawing.Size(135, 20);
            this.textBoxRCEFontSize.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Font size:";
            // 
            // labelScriptEditorBackground
            // 
            this.labelScriptEditorBackground.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorBackground.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorBackground.Location = new System.Drawing.Point(203, 99);
            this.labelScriptEditorBackground.Name = "labelScriptEditorBackground";
            this.labelScriptEditorBackground.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorBackground.TabIndex = 12;
            // 
            // comboBoxScriptEditorBackground
            // 
            this.comboBoxScriptEditorBackground.FormattingEnabled = true;
            this.comboBoxScriptEditorBackground.Location = new System.Drawing.Point(90, 99);
            this.comboBoxScriptEditorBackground.Name = "comboBoxScriptEditorBackground";
            this.comboBoxScriptEditorBackground.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorBackground.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Background:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelScriptEditorSynBools);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.comboBoxScriptEditorSynBools);
            this.groupBox5.Controls.Add(this.labelScriptEditorSynIntegers);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.comboBoxScriptEditorSynIntegers);
            this.groupBox5.Controls.Add(this.labelScriptEditorSynStrings);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.comboBoxScriptEditorSynStrings);
            this.groupBox5.Controls.Add(this.labelScriptEditorSynRegular);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.comboBoxScriptEditorSynRegular);
            this.groupBox5.Controls.Add(this.labelScriptEditorSynFunctionsN);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.comboBoxScriptEditorSynFunctionsN);
            this.groupBox5.Controls.Add(this.labelScriptEditorSynFunctionsA);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.comboBoxScriptEditorSynFunctionsA);
            this.groupBox5.Controls.Add(this.labelScriptEditorSynFunctions);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.comboBoxScriptEditorSynFunctions);
            this.groupBox5.Controls.Add(this.labelScriptEditorSynComments);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.comboBoxScriptEditorSynComments);
            this.groupBox5.Location = new System.Drawing.Point(259, 148);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(238, 249);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Script Editor (Syntax Highlighting)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Comments:";
            // 
            // labelScriptEditorSynComments
            // 
            this.labelScriptEditorSynComments.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorSynComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorSynComments.Location = new System.Drawing.Point(203, 53);
            this.labelScriptEditorSynComments.Name = "labelScriptEditorSynComments";
            this.labelScriptEditorSynComments.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorSynComments.TabIndex = 15;
            // 
            // comboBoxScriptEditorSynComments
            // 
            this.comboBoxScriptEditorSynComments.FormattingEnabled = true;
            this.comboBoxScriptEditorSynComments.Location = new System.Drawing.Point(90, 53);
            this.comboBoxScriptEditorSynComments.Name = "comboBoxScriptEditorSynComments";
            this.comboBoxScriptEditorSynComments.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorSynComments.TabIndex = 16;
            // 
            // labelScriptEditorSynFunctions
            // 
            this.labelScriptEditorSynFunctions.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorSynFunctions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorSynFunctions.Location = new System.Drawing.Point(203, 80);
            this.labelScriptEditorSynFunctions.Name = "labelScriptEditorSynFunctions";
            this.labelScriptEditorSynFunctions.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorSynFunctions.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "Functions:";
            // 
            // comboBoxScriptEditorSynFunctions
            // 
            this.comboBoxScriptEditorSynFunctions.FormattingEnabled = true;
            this.comboBoxScriptEditorSynFunctions.Location = new System.Drawing.Point(90, 80);
            this.comboBoxScriptEditorSynFunctions.Name = "comboBoxScriptEditorSynFunctions";
            this.comboBoxScriptEditorSynFunctions.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorSynFunctions.TabIndex = 19;
            // 
            // labelScriptEditorSynFunctionsA
            // 
            this.labelScriptEditorSynFunctionsA.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorSynFunctionsA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorSynFunctionsA.Location = new System.Drawing.Point(203, 107);
            this.labelScriptEditorSynFunctionsA.Name = "labelScriptEditorSynFunctionsA";
            this.labelScriptEditorSynFunctionsA.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorSynFunctionsA.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 110);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "Functions A:";
            // 
            // comboBoxScriptEditorSynFunctionsA
            // 
            this.comboBoxScriptEditorSynFunctionsA.FormattingEnabled = true;
            this.comboBoxScriptEditorSynFunctionsA.Location = new System.Drawing.Point(90, 107);
            this.comboBoxScriptEditorSynFunctionsA.Name = "comboBoxScriptEditorSynFunctionsA";
            this.comboBoxScriptEditorSynFunctionsA.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorSynFunctionsA.TabIndex = 22;
            // 
            // labelScriptEditorSynFunctionsN
            // 
            this.labelScriptEditorSynFunctionsN.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorSynFunctionsN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorSynFunctionsN.Location = new System.Drawing.Point(203, 134);
            this.labelScriptEditorSynFunctionsN.Name = "labelScriptEditorSynFunctionsN";
            this.labelScriptEditorSynFunctionsN.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorSynFunctionsN.TabIndex = 23;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 137);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Functions N:";
            // 
            // comboBoxScriptEditorSynFunctionsN
            // 
            this.comboBoxScriptEditorSynFunctionsN.FormattingEnabled = true;
            this.comboBoxScriptEditorSynFunctionsN.Location = new System.Drawing.Point(90, 134);
            this.comboBoxScriptEditorSynFunctionsN.Name = "comboBoxScriptEditorSynFunctionsN";
            this.comboBoxScriptEditorSynFunctionsN.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorSynFunctionsN.TabIndex = 25;
            // 
            // labelScriptEditorSynRegular
            // 
            this.labelScriptEditorSynRegular.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorSynRegular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorSynRegular.Location = new System.Drawing.Point(203, 26);
            this.labelScriptEditorSynRegular.Name = "labelScriptEditorSynRegular";
            this.labelScriptEditorSynRegular.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorSynRegular.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 29);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Regular:";
            // 
            // comboBoxScriptEditorSynRegular
            // 
            this.comboBoxScriptEditorSynRegular.FormattingEnabled = true;
            this.comboBoxScriptEditorSynRegular.Location = new System.Drawing.Point(90, 26);
            this.comboBoxScriptEditorSynRegular.Name = "comboBoxScriptEditorSynRegular";
            this.comboBoxScriptEditorSynRegular.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorSynRegular.TabIndex = 28;
            // 
            // labelScriptEditorSynStrings
            // 
            this.labelScriptEditorSynStrings.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorSynStrings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorSynStrings.Location = new System.Drawing.Point(203, 161);
            this.labelScriptEditorSynStrings.Name = "labelScriptEditorSynStrings";
            this.labelScriptEditorSynStrings.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorSynStrings.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 164);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "String:";
            // 
            // comboBoxScriptEditorSynStrings
            // 
            this.comboBoxScriptEditorSynStrings.FormattingEnabled = true;
            this.comboBoxScriptEditorSynStrings.Location = new System.Drawing.Point(90, 161);
            this.comboBoxScriptEditorSynStrings.Name = "comboBoxScriptEditorSynStrings";
            this.comboBoxScriptEditorSynStrings.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorSynStrings.TabIndex = 31;
            // 
            // labelScriptEditorSynIntegers
            // 
            this.labelScriptEditorSynIntegers.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorSynIntegers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorSynIntegers.Location = new System.Drawing.Point(203, 188);
            this.labelScriptEditorSynIntegers.Name = "labelScriptEditorSynIntegers";
            this.labelScriptEditorSynIntegers.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorSynIntegers.TabIndex = 32;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 191);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Integers:";
            // 
            // comboBoxScriptEditorSynIntegers
            // 
            this.comboBoxScriptEditorSynIntegers.FormattingEnabled = true;
            this.comboBoxScriptEditorSynIntegers.Location = new System.Drawing.Point(90, 188);
            this.comboBoxScriptEditorSynIntegers.Name = "comboBoxScriptEditorSynIntegers";
            this.comboBoxScriptEditorSynIntegers.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorSynIntegers.TabIndex = 34;
            // 
            // labelScriptEditorSynBools
            // 
            this.labelScriptEditorSynBools.BackColor = System.Drawing.Color.DarkRed;
            this.labelScriptEditorSynBools.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScriptEditorSynBools.Location = new System.Drawing.Point(203, 215);
            this.labelScriptEditorSynBools.Name = "labelScriptEditorSynBools";
            this.labelScriptEditorSynBools.Size = new System.Drawing.Size(22, 21);
            this.labelScriptEditorSynBools.TabIndex = 35;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 218);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 13);
            this.label20.TabIndex = 36;
            this.label20.Text = "Bools:";
            // 
            // comboBoxScriptEditorSynBools
            // 
            this.comboBoxScriptEditorSynBools.FormattingEnabled = true;
            this.comboBoxScriptEditorSynBools.Location = new System.Drawing.Point(90, 215);
            this.comboBoxScriptEditorSynBools.Name = "comboBoxScriptEditorSynBools";
            this.comboBoxScriptEditorSynBools.Size = new System.Drawing.Size(107, 21);
            this.comboBoxScriptEditorSynBools.TabIndex = 37;
            // 
            // OptionsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 506);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonClearLogin);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RCE - Options";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxDisplayWeaponListView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxDisplayClassListView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxScriptEditorIndentSize;
        private System.Windows.Forms.TextBox textBoxScriptEditorFontSize;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonClearLogin;
        public System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxObjBrowserColourCoding;
        private System.Windows.Forms.CheckBox checkBoxObjBrowserHideGraalFuncs;
        private System.Windows.Forms.CheckBox checkBoxVisibleByDefault;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxRCEFontFamily;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxRCEFontSize;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxRCEPrefixColour;
        private System.Windows.Forms.Label labelRCEPrefixColour;
        private System.Windows.Forms.Label labelScriptEditorBackground;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorBackground;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label labelScriptEditorSynFunctions;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorSynFunctions;
        private System.Windows.Forms.Label labelScriptEditorSynComments;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorSynComments;
        private System.Windows.Forms.Label labelScriptEditorSynFunctionsN;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorSynFunctionsN;
        private System.Windows.Forms.Label labelScriptEditorSynFunctionsA;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorSynFunctionsA;
        private System.Windows.Forms.Label labelScriptEditorSynRegular;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorSynRegular;
        private System.Windows.Forms.Label labelScriptEditorSynBools;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorSynBools;
        private System.Windows.Forms.Label labelScriptEditorSynIntegers;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorSynIntegers;
        private System.Windows.Forms.Label labelScriptEditorSynStrings;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBoxScriptEditorSynStrings;

    }
}