﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteControlE
{
    public partial class NewScriptWindow : Form
    {
        public NewScriptWindow()
        {
            InitializeComponent();

            buttonCreate.Click += (sender, e) => { this.AttemptClose(); };
            textBoxName.KeyDown += (sender, e) =>
            {
                if (e.KeyCode == Keys.Enter)
                    this.AttemptClose();
            };
        }

        private void AttemptClose()
        {
            if (String.IsNullOrEmpty(textBoxName.Text.Trim()) == false)
                this.Close();
        }

        public string Prompt()
        {
            this.ShowDialog();
            return textBoxName.Text;
        }
    }
}
