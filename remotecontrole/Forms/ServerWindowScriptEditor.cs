﻿using FastColoredTextBoxNS;
using OpenGraal.Core;
using OpenGraal.GraalIM;
using OpenGraal.NpcServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using RemoteControlE.Models;

namespace RemoteControlE
{
    public partial class ServerWindowScriptEditor : DockContent
    {
        DockPanel dockPanel;
        public string scriptName;
        public ScriptType scriptType;
        public int scriptId;
        int ticker;

        ServerWindowScriptEditorObjects objectsWindow;
        ServerWindowScriptEditorEditor editorWindow;

        public enum ScriptType
        {
            WEAPON,
            CLASS,
            NPC
        };

        public ServerWindowScriptEditor(string name, ScriptType type)
        {
            InitializeComponent();

            scriptName = name;
            scriptType = type;

            #region GUI
            this.Text = String.Format("{0}: {1}", ScriptTypeToString(type), name);

            dockPanel = new DockPanel();
            dockPanel.Dock = DockStyle.Fill;
            dockPanel.DocumentStyle = DocumentStyle.DockingWindow;
            dockPanel.Theme = new WeifenLuo.WinFormsUI.Docking.VS2012LightTheme();
            dockPanel.DockRightPortion = 280;

            objectsWindow = new ServerWindowScriptEditorObjects();
            objectsWindow.CloseButton = false;
            objectsWindow.CloseButtonVisible = false;
            objectsWindow.Text = "Browser";
            objectsWindow.buttonRefresh.Click += buttonRefresh_Click;

            // Auto hide object browser based on setting
            if (Settings.GetSetting("ObjBrowserVisibleByDefault") == "True")
                objectsWindow.Show(dockPanel, DockState.DockRight);
            else objectsWindow.Show(dockPanel, DockState.DockRightAutoHide);

            editorWindow = new ServerWindowScriptEditorEditor();
            editorWindow.CloseButton = false;
            editorWindow.CloseButtonVisible = false;
            editorWindow.Text = "Editor";

            editorWindow.ResetBackColor();

            editorWindow.Show(dockPanel, DockState.Document);

            this.Controls.Add(dockPanel);
            this.buttonSave.Click += buttonSave_Click;
            #endregion
        }

        void buttonRefresh_Click(object sender, EventArgs e)
        {
            BuildObjectMap();
        }


        public void BuildObjectMap()
        {
            objectsWindow.treeView1.Nodes.Clear();

            TreeNode nodeVars = new TreeNode("Variables");
            TreeNode nodeFunctions = new TreeNode("Functions");

            for (int i = 0; i < editorWindow.scriptEditor.Lines.Count; i++)
            {
                string line = editorWindow.scriptEditor.Lines[i];
                if (line.StartsWith("function") && (line.EndsWith("{") || editorWindow.scriptEditor.Lines[i + 1] == "{"))
                {
                    ObjectBrowserFunction function = new ObjectBrowserFunction();
                    function.Name = line.Substring(("function ").Length);
                    function.Name = function.Name.Substring(0, function.Name.IndexOf("("));
                    function.Line = i;

                    TreeNode node = new TreeNode(String.Format("{0} : {1}", function.Name, function.Line.ToString()));

                    // Apply colour coding if setting
                    if (Settings.GetSetting("ObjBrowserColourCoding") == "True")
                    {
                        if (function.Name.StartsWith("on"))
                            node.ForeColor = Color.Red;
                        else node.ForeColor = Color.Green;
                    }

                    string argsStr = line.Substring(line.IndexOf("(") + 1);
                    argsStr = argsStr.Substring(0, argsStr.IndexOf(")"));

                    // Ignore functions with no arguments
                    if (argsStr.Trim().Length > 0)
                    {
                        string[] args = argsStr.Split(',');
                        foreach (string arg in args)
                        {
                            string argTrimmed = arg.TrimStart();
                            node.Nodes.Add(argTrimmed);
                        }

                        Console.WriteLine("----");
                    }

                    nodeFunctions.Nodes.Add(node);
                }
            }

            objectsWindow.treeView1.Nodes.Add(nodeVars);
            objectsWindow.treeView1.Nodes.Add(nodeFunctions);
            nodeFunctions.Expand();
        }

        void buttonSave_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(scriptName)) return;

            NCConnection.PacketIn sendType;
            switch (this.scriptType)
            {
                case ScriptType.CLASS: sendType = NCConnection.PacketIn.NC_CLASSADD; break;
                case ScriptType.WEAPON: sendType = NCConnection.PacketIn.NC_WEAPONADD; break;
                case ScriptType.NPC: sendType = NCConnection.PacketIn.NC_NPCADD; break;
                default: return;
            }

            Information.NcConnection.SendPacket(new CString() + (byte)sendType + (byte)scriptName.Length + scriptName + (byte)0 + "" + editorWindow.scriptEditor.Text.Replace('\n', '§'), true);
            Errors.DumpFile(String.Format("{0}_{1}.txt", ScriptTypeToString(this.scriptType), this.scriptName.Replace('/', '_')), editorWindow.scriptEditor.Text.Replace('\n', '§'));
        }

        public string ScriptTypeToString(ScriptType type)
        {
            switch (type)
            {
                case ScriptType.WEAPON:
                    return "Weapon";
                case ScriptType.CLASS:
                    return "Class";
                case ScriptType.NPC:
                    return "NPC";
                default:
                    return null;
            }
        }

        public void SetText(string text)
        {
            editorWindow.scriptEditor.Text = text;
            BuildObjectMap();
        }
    }
}
