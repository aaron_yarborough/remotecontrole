﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteControlE
{
    public partial class AppInfoWindow : Form
    {
        public AppInfoWindow()
        {
            InitializeComponent();

            label2.Text = "Version: " + Application.ProductVersion;

            return;
        }
    }
}
