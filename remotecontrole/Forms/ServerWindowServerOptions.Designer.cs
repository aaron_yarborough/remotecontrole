﻿namespace RemoteControlE
{
    partial class ServerWindowServerOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerWindowServerOptions));
            this.buttonSave = new System.Windows.Forms.Button();
            this.fastColoredTextBoxContent = new FastColoredTextBoxNS.FastColoredTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBoxContent)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(441, 476);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(108, 23);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Save changes";
            this.buttonSave.UseVisualStyleBackColor = true;
            // 
            // fastColoredTextBoxContent
            // 
            this.fastColoredTextBoxContent.AllowDrop = false;
            this.fastColoredTextBoxContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fastColoredTextBoxContent.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.fastColoredTextBoxContent.AutoScrollMinSize = new System.Drawing.Size(107, 14);
            this.fastColoredTextBoxContent.BackBrush = null;
            this.fastColoredTextBoxContent.CharHeight = 14;
            this.fastColoredTextBoxContent.CharWidth = 8;
            this.fastColoredTextBoxContent.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fastColoredTextBoxContent.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.fastColoredTextBoxContent.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.fastColoredTextBoxContent.IsReplaceMode = false;
            this.fastColoredTextBoxContent.Location = new System.Drawing.Point(0, 0);
            this.fastColoredTextBoxContent.Name = "fastColoredTextBoxContent";
            this.fastColoredTextBoxContent.Paddings = new System.Windows.Forms.Padding(0);
            this.fastColoredTextBoxContent.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.fastColoredTextBoxContent.Size = new System.Drawing.Size(561, 470);
            this.fastColoredTextBoxContent.TabIndex = 2;
            this.fastColoredTextBoxContent.Text = "Loading...";
            this.fastColoredTextBoxContent.Zoom = 100;
            // 
            // ServerWindowServerOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 511);
            this.Controls.Add(this.fastColoredTextBoxContent);
            this.Controls.Add(this.buttonSave);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ServerWindowServerOptions";
            this.ShowIcon = false;
            this.Text = "RCE - Serveroptions";
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBoxContent)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonSave;
        private FastColoredTextBoxNS.FastColoredTextBox fastColoredTextBoxContent;
    }
}