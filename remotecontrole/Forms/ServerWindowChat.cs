﻿using OpenGraal.GraalIM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindowChat : DockContent
    {

        public ServerWindowChat()
        {
            InitializeComponent();

            textBoxChatBar.KeyDown += textBox1_KeyDown;
            richTextBoxChat.LinkClicked += richTextBoxChat_LinkClicked;
        }

        void richTextBoxChat_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }

        void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;

                if (textBoxChatBar.Text.Trim() == "") return;

                Information.Framework.SendRC_CHAT(textBoxChatBar.Text);
                textBoxChatBar.Text = "";
            }
        }

        public void AddIRCTab(string channel)
        {
            if (tabControl1.InvokeRequired)
            {
                tabControl1.Invoke(new Action<string>(AddIRCTab), channel);
                return;
            }

            TabPage newTab = new TabPage();
            newTab.Text = channel;

            tabControl1.Controls.Add(newTab);
        }

        public void UpdateRCChat(string text)
        {
            if (richTextBoxChat.InvokeRequired)
            {
                richTextBoxChat.Invoke(new Action<string>(UpdateRCChat), text);
                return;
            }

            if (richTextBoxChat.Text != "")
                text = Environment.NewLine + text;

            if (text.IndexOf(":") > -1)
            {
                richTextBoxChat.SelectionStart = richTextBoxChat.Text.Length;

                Color color = Color.Green;
                try
                {
                    color = Color.FromName(Settings.GetSetting("RCEChatPrefixColour"));
                }
                catch (Exception ex)
                {
                    Errors.Log(ex);
                }
                
                richTextBoxChat.SelectionColor = color;
                richTextBoxChat.SelectionFont = new Font(
                    richTextBoxChat.SelectionFont.FontFamily,
                    richTextBoxChat.SelectionFont.Size,
                    FontStyle.Bold
                );
                richTextBoxChat.AppendText(@text.Substring(0, text.IndexOf(":") + 1));

                richTextBoxChat.SelectionColor = Color.Black;
                richTextBoxChat.SelectionFont = new Font(
                    richTextBoxChat.SelectionFont.FontFamily,
                    richTextBoxChat.SelectionFont.Size,
                    FontStyle.Regular
                );
                richTextBoxChat.AppendText(@text.Substring(text.IndexOf(":") + 1));
            }
            else richTextBoxChat.AppendText(text);

            richTextBoxChat.SelectionStart = richTextBoxChat.Text.Length;
            richTextBoxChat.ScrollToCaret();
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
