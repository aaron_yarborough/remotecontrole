﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenGraal.GraalIM;
using System.Threading;
using OpenGraal.GraalIM.Connections;
using System.IO;
using Newtonsoft.Json;
using RemoteControlE.Models;

namespace RemoteControlE
{
    public partial class LoginWindow : Form
    {
        ListServerConnection _lsc = new ListServerConnection();

        public LoginWindow()
        {
            InitializeComponent();

            CreateFiles();

            buttonLogin.Click += buttonLogin_Click;

            Login login = GetLoginDetailsFromFile();
            if (login != null)
            {
                textBoxNickname.Text = login.Nickname;
                textBoxAccount.Text = login.Account;
                textBoxPassword.Text = login.Password;
            }
        }

        private void CreateFiles()
        {
            string dir = Helpers.GetAppDataDir();

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            if (!File.Exists(Path.Combine(dir, "data.settings.json")))
                File.Create(Path.Combine(dir, "data.settings.json"));
            if (!File.Exists(Path.Combine(dir, "data.login.json")))
                File.Create(Path.Combine(dir, "data.login.json"));
        }

        private void SaveLoginDetailsToFile(string nickname, string account, string password)
        {
            Login login = new Login();
            login.Nickname = nickname;
            login.Account = account;
            login.Password = password;
            
            try
            {
                string json = JsonConvert.SerializeObject(login);
                File.WriteAllText(Path.Combine(Helpers.GetAppDataDir(), "data.login.json"), json);
            }
            catch (Exception ex)
            {
                Errors.Log(ex);
                MessageBox.Show("Your login details could not be saved!", "Error");
            }
        }

        private Login GetLoginDetailsFromFile()
        {
            string path = Path.Combine(Helpers.GetAppDataDir(), "data.login.json");

            if (!File.Exists(path))
                return null;

            string json = File.ReadAllText(path);
            Login login = JsonConvert.DeserializeObject<Login>(json);

            return login;
        }

        void buttonLogin_Click(object sender, EventArgs e)
        {
            buttonLogin.Enabled = false;

            string nickname = textBoxNickname.Text;
            string account = textBoxAccount.Text;
            string password = textBoxPassword.Text;

            SaveLoginDetailsToFile(nickname, account, password);

            if (String.IsNullOrEmpty(nickname) || String.IsNullOrEmpty(account) || String.IsNullOrEmpty(password))
            {
                buttonLogin.Enabled = true;
                MessageBox.Show("Please provide a nickname, account name and password!", "Alert");
                return;
            }

            if (nickname.Length < 3)
            {
                buttonLogin.Enabled = true;
                MessageBox.Show("Your nickname must be more than 3 characters long!", "Alert");
                return;
            }

            if (ConnectToListserver() == true)
            {
                AttemptLogin(nickname, account, password);
            }
        }

        int _loginAttempts = 0;
        private bool ConnectToListserver()
        {
            _lsc = new ListServerConnection();

            try
            {
                _lsc.Connect("listserver.graalonline.com", 14922);
            }
            catch (Exception ex)
            {
                if (_loginAttempts >= 5)
                {
                    MessageBox.Show("Couldn't connect to the listserver after 5 attempts!", "Error");
                    Environment.Exit(0);
                    return false;
                }

                _loginAttempts++;
                Thread.Sleep(1000);

                ConnectToListserver();
            }

            return true;
        }

        void AttemptLogin(string nickname, string account, string password)
        {
            Information.Nickname = nickname;
            Information.Account = account;
            Information.Password = password;

            _lsc.SendLogin(account, password, nickname);
            _lsc.ReceiveData();

            int num = 5000;
            while (_lsc.errormsg == "" && _lsc.n_servers == "")
            {
                if (num <= 0)
                {
                    MessageBox.Show("There was an issue connection to the listserver!");
                    buttonLogin.Enabled = true;
                    break;
                }

                num--;
                Thread.Sleep(100);
            }

            // Don't open the server list if an error occured while
            // connection to the server
            if (_lsc.errormsg != "")
                return;

            ServerListWindow serverListWindow = new ServerListWindow(_lsc.serverList);
            serverListWindow.Show();

            this.Hide();
        }
    }
}
