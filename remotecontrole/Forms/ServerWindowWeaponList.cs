﻿using OpenGraal.Core;
using OpenGraal.GraalIM;
using OpenGraal.GraalIM.Connections;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindowWeaponList : DockContent
    {
        delegate void AddWeaponCallback(string name);
        delegate void AddWeaponListCallback(List<string> weapons);

        List<string> _weaponList = new List<string>();

        public ServerWindowWeaponList()
        {
            InitializeComponent();

            this.buttonCreate.Click += buttonCreate_Click;
            this.FormClosing += ServerWindowWeaponList_FormClosing;
            this.listBoxWeapons.MouseDoubleClick += listBoxWeapons_MouseDoubleClick;
        }

        void listBoxWeapons_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBoxWeapons.SelectedItems.Count == 0) return;

            string weapon = listBoxWeapons.GetItemText(listBoxWeapons.SelectedItem);
            Information.NcConnection.GetWeaponContent(weapon);
        }

        void ServerWindowWeaponList_FormClosing(object sender, FormClosingEventArgs e)
        {
            listBoxWeapons.Items.Clear();
            e.Cancel = true;
            this.Hide();
        }

        void buttonCreate_Click(object sender, EventArgs e)
        {
            NewScriptWindow newScriptWindow = new NewScriptWindow();
            string weaponName = newScriptWindow.Prompt();

            ServerWindowScriptEditor editor = new ServerWindowScriptEditor(weaponName, ServerWindowScriptEditor.ScriptType.WEAPON);
            editor.Show(Information.serverWindow.dockPanel, DockState.Document);
        }

        public void AddWeapon(string name)
        {
            if (this.InvokeRequired)
            {
                AddWeaponCallback callback = new AddWeaponCallback(AddWeapon);
                this.Invoke(callback, name);
            }

            try
            {
                listBoxWeapons.Items.Add(name);
            }
            catch { }
        }

        public void AddWeaponList(List<string> weapons)
        {
            if (listBoxWeapons.InvokeRequired)
            {
                AddWeaponListCallback callback = new AddWeaponListCallback(AddWeaponList);
                this.Invoke(callback, weapons);
            }

            try
            {
                foreach (string weapon in weapons)
                {
                    // Avoid adding duplicate entries
                    // TODO: Look into what is causing this
                    if (_weaponList.Contains(weapon))
                        continue;

                    listBoxWeapons.Items.Add(weapon);
                    _weaponList.Add(weapon);
                }

                listBoxWeapons.Sorted = true;
            }
            catch { }
        }
    }
}
