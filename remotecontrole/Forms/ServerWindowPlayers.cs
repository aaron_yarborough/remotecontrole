﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using OpenGraal.Common.Players;
using System.Threading;

namespace RemoteControlE
{
    public partial class ServerWindowPlayers : DockContent
    {
        List<string> players = new List<string>();
        List<string> rcPlayers = new List<string>();

        public ServerWindowPlayers()
        {
            InitializeComponent();

            listViewPlayers.Sorting = SortOrder.Ascending;
            listViewRCPlayers.Sorting = SortOrder.Ascending;
        }

        delegate void AddPlayerCallback(string nickname, string account, string level);

        public void AddPlayer(string nickname, string account, string level)
        {
            if (listViewPlayers.InvokeRequired)
            {
                AddPlayerCallback callback = new AddPlayerCallback(AddPlayer);
                this.Invoke(callback, nickname, account, level);
            }

            try
            {
                if (String.IsNullOrEmpty(level))
                {
                    if (rcPlayers.Contains(account)) return;

                    listViewRCPlayers.Items.Add(new ListViewItem(new string[] { nickname, account }));
                    listViewRCPlayers.Sort();
                    rcPlayers.Add(account);
                }
                else
                {
                    if (players.Contains(account)) return;

                    listViewPlayers.Items.Add(new ListViewItem(new string[] { nickname, account, level }));
                    listViewPlayers.Sort();
                    players.Add(account);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
