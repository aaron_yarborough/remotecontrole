﻿namespace RemoteControlE
{
    partial class ServerWindowComments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerWindowComments));
            this.fastColoredTextBoxComments = new FastColoredTextBoxNS.FastColoredTextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBoxComments)).BeginInit();
            this.SuspendLayout();
            // 
            // fastColoredTextBoxComments
            // 
            this.fastColoredTextBoxComments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fastColoredTextBoxComments.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.fastColoredTextBoxComments.AutoScrollMinSize = new System.Drawing.Size(27, 14);
            this.fastColoredTextBoxComments.BackBrush = null;
            this.fastColoredTextBoxComments.CharHeight = 14;
            this.fastColoredTextBoxComments.CharWidth = 8;
            this.fastColoredTextBoxComments.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fastColoredTextBoxComments.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.fastColoredTextBoxComments.IsReplaceMode = false;
            this.fastColoredTextBoxComments.Location = new System.Drawing.Point(0, 0);
            this.fastColoredTextBoxComments.Name = "fastColoredTextBoxComments";
            this.fastColoredTextBoxComments.Paddings = new System.Windows.Forms.Padding(0);
            this.fastColoredTextBoxComments.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.fastColoredTextBoxComments.Size = new System.Drawing.Size(512, 373);
            this.fastColoredTextBoxComments.TabIndex = 0;
            this.fastColoredTextBoxComments.Zoom = 100;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(424, 379);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(76, 25);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            // 
            // ServerWindowComments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 416);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.fastColoredTextBoxComments);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ServerWindowComments";
            this.Text = "RCE - Comments: (none)";
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBoxComments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public FastColoredTextBoxNS.FastColoredTextBox fastColoredTextBoxComments;
        public System.Windows.Forms.Button buttonSave;
    }
}