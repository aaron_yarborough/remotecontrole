﻿using OpenGraal.Core;
using OpenGraal.GraalIM;
using OpenGraal.GraalIM.Connections;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindowServerOptions : DockContent
    {
        public ServerWindowServerOptions()
        {
            InitializeComponent();

            this.FormClosing += ServerWindowServerOptions_FormClosing;
        }

        void ServerWindowServerOptions_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        delegate void SetTextCallback(string text);

        public void SetText(string text)
        {
            if (fastColoredTextBoxContent.InvokeRequired)
            {
                this.Invoke(new SetTextCallback(SetText), text);
            }

            try
            {
                fastColoredTextBoxContent.Text = text;
            }
            catch (Exception ex)
            {
                return;
            }
        }
    }
}
