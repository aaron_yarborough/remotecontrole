﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using RemoteControlE.Models;
using Newtonsoft.Json;
using System.Collections;
using System.Reflection;

namespace RemoteControlE
{
    public partial class OptionsWindow : DockContent
    {
        public OptionsWindow()
        {
            InitializeComponent();

            #region Events
            this.buttonClearLogin.Click += buttonClearLogin_Click;
            this.buttonSave.Click += buttonSave_Click;
            this.Shown += OptionsWindow_Shown;
            this.FormClosing += OptionsWindow_FormClosing;
            this.comboBoxRCEPrefixColour.TextChanged += comboBoxRCEPrefixColour_TextChanged;
            this.comboBoxScriptEditorBackground.TextChanged += comboBoxScriptEditorBackground_TextChanged;
            this.comboBoxScriptEditorSynRegular.TextChanged += comboBoxScriptEditorSynRegular_TextChanged;
            this.comboBoxScriptEditorSynComments.TextChanged += comboBoxScriptEditorSynComments_TextChanged;
            this.comboBoxScriptEditorSynFunctions.TextChanged += comboBoxScriptEditorSynFunctions_TextChanged;
            #endregion

            LoadColoursToList();
        }

        void comboBoxScriptEditorSynFunctions_TextChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void comboBoxScriptEditorSynComments_TextChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void comboBoxScriptEditorSynRegular_TextChanged(object sender, EventArgs e)
        {
            labelScriptEditorSynRegular.BackColor = Color.FromName(comboBoxScriptEditorSynRegular.Text);
        }

        #region Event handlers
        void comboBoxScriptEditorBackground_TextChanged(object sender, EventArgs e)
        {
            labelScriptEditorBackground.BackColor = Color.FromName(comboBoxScriptEditorBackground.Text);
        }

        void comboBoxRCEPrefixColour_TextChanged(object sender, EventArgs e)
        {
            labelRCEPrefixColour.BackColor = Color.FromName(comboBoxRCEPrefixColour.Text);
        }

        void OptionsWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.HideOnClose = true;
        }

        void OptionsWindow_Shown(object sender, EventArgs e)
        {
            LoadSettings();
        }

        void buttonSave_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }

        void buttonClearLogin_Click(object sender, EventArgs e)
        {
            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "RemoteControlE", "data.login.json"), "");
            MessageBox.Show("Successfully cleared login details!", "Success");
        }
        #endregion

        /// <summary>
        /// Loads setting changes from file and into window
        /// </summary>
        private void LoadSettings()
        {
            Dictionary<string, string> settings = Settings.GetSettings();
            try
            {
                comboBoxDisplayWeaponListView.Text = settings["DisplayWeaponListView"];
                comboBoxDisplayClassListView.Text = settings["DisplayClassListView"];

                textBoxScriptEditorFontSize.Text = settings["ScriptEditorFontSize"];
                textBoxScriptEditorIndentSize.Text = settings["ScriptEditorIndentSize"];
                textBoxRCEFontFamily.Text = settings["ScriptEditorFontFamily"];
                comboBoxScriptEditorBackground.Text = settings["ScriptEditorBackgroundColour"];

                comboBoxScriptEditorSynRegular.Text = settings["ScriptEditorSynRegular"];
                comboBoxScriptEditorSynFunctions.Text = settings["ScriptEditorSynFunctions"];

                checkBoxObjBrowserColourCoding.Checked = Convert.ToBoolean(settings["ObjBrowserColourCoding"]);
                checkBoxObjBrowserHideGraalFuncs.Checked = Convert.ToBoolean(settings["ObjBrowserHideGraalFuncs"]);
                checkBoxVisibleByDefault.Checked = Convert.ToBoolean(settings["ObjBrowserVisibleByDefault"]);

                textBoxRCEFontSize.Text = settings["RCEChatFontSize"];
                textBoxRCEFontFamily.Text = settings["RCEChatFontFamily"];
                comboBoxRCEPrefixColour.Text = settings["RCEChatPrefixColour"];
            }
            catch (Exception ex)
            {
                Errors.Log(ex);
            }
        }

        /// <summary>
        /// Saves setting changes to file
        /// </summary>
        private void SaveChanges()
        {
            Dictionary<string, string> settings = new Dictionary<string, string>();
            settings.Add("DisplayWeaponListView", comboBoxDisplayWeaponListView.Text);
            settings.Add("DisplayClassListView", comboBoxDisplayClassListView.Text);

            settings.Add("ScriptEditorFontSize", textBoxScriptEditorFontSize.Text);
            settings.Add("ScriptEditorIndentSize", textBoxScriptEditorIndentSize.Text);
            settings.Add("ScriptEditorFontFamily", textBoxRCEFontFamily.Text);
            settings.Add("ScriptEditorBackgroundColour", comboBoxScriptEditorBackground.Text);

            settings.Add("ScriptEditorSynRegular", comboBoxScriptEditorSynRegular.Text);

            settings.Add("ObjBrowserColourCoding", checkBoxObjBrowserColourCoding.Checked.ToString());
            settings.Add("ObjBrowserHideGraalFuncs", checkBoxObjBrowserHideGraalFuncs.Checked.ToString());
            settings.Add("ObjBrowserVisibleByDefault", checkBoxVisibleByDefault.Checked.ToString());

            settings.Add("RCEChatFontSize", textBoxRCEFontSize.Text);
            settings.Add("RCEChatFontFamily", textBoxRCEFontFamily.Text);
            settings.Add("RCEChatPrefixColour", comboBoxRCEPrefixColour.Text);

            // TODO: Add code for applying settings across application
            // without restart

            string json = JsonConvert.SerializeObject(settings, Formatting.Indented);
            try
            {
                File.WriteAllText(Path.Combine(Helpers.GetAppDataDir(), "data.settings.json"), json);
                this.Close();
            }
            catch (Exception ex)
            {
                Errors.Log(ex);
                MessageBox.Show("An error occured while saving your settings!", "Error");
            }
        }

        /// <summary>
        /// Populates prefix colour dropbown with .NET colours
        /// </summary>
        private void LoadColoursToList()
        {
            ArrayList ColorList = new ArrayList();
            Type colorType = typeof(System.Drawing.Color);
            PropertyInfo[] propInfoList = colorType.GetProperties(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public);
            foreach (var colour in propInfoList)
            {
                comboBoxRCEPrefixColour.Items.Add(colour.Name);
                comboBoxScriptEditorBackground.Items.Add(colour.Name);
                comboBoxScriptEditorSynBools.Items.Add(colour.Name);
                comboBoxScriptEditorSynComments.Items.Add(colour.Name);
                comboBoxScriptEditorSynFunctions.Items.Add(colour.Name);
                comboBoxScriptEditorSynFunctionsA.Items.Add(colour.Name);
                comboBoxScriptEditorSynFunctionsN.Items.Add(colour.Name);
                comboBoxScriptEditorSynIntegers.Items.Add(colour.Name);
                comboBoxScriptEditorSynRegular.Items.Add(colour.Name);
                comboBoxScriptEditorSynStrings.Items.Add(colour.Name);
            }
        }
    }
}
