﻿using FastColoredTextBoxNS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindowScriptEditorEditor : DockContent
    {
        public FastColoredTextBox scriptEditor;

        public Style CommentStyle;
        public Style FuncStyle;
        public Style AFuncStyle;
        public Style NFuncStyle;
        public Style StringStyle;
        public Style IntStyle;
        public Style BoolStyle;

        public ServerWindowScriptEditorEditor()
        {
            InitializeComponent();

            CommentStyle = new TextStyle(Brushes.Green, null, FontStyle.Regular);
            FuncStyle = new TextStyle(new SolidBrush(Helpers.GetColorFromString(Settings.GetSetting("ScriptEditorSynFunctions"))), null, FontStyle.Regular);
            AFuncStyle = new TextStyle(new SolidBrush(Helpers.GetColorFromString(Settings.GetSetting("ScriptEditorSynFunctionsA"))), null, FontStyle.Regular);
            NFuncStyle = new TextStyle(new SolidBrush(Helpers.GetColorFromString(Settings.GetSetting("ScriptEditorSynFunctionsN"))), null, FontStyle.Regular);
            StringStyle = new TextStyle(new SolidBrush(Helpers.GetColorFromString(Settings.GetSetting("ScriptEditorSynStrings"))), null, FontStyle.Regular);
            IntStyle = new TextStyle(new SolidBrush(Helpers.GetColorFromString(Settings.GetSetting("ScriptEditorSynIntegers"))), null, FontStyle.Regular);
            BoolStyle = new TextStyle(Brushes.Blue, null, FontStyle.Regular);

            scriptEditor = new FastColoredTextBox();
            scriptEditor.Dock = DockStyle.Fill;
            scriptEditor.TextChanged += scriptEditor_TextChanged;
            scriptEditor.TabLength = 2;

            float settingFontSize = float.Parse(Settings.GetSetting("ScriptEditorFontSize"), CultureInfo.InvariantCulture.NumberFormat);
            string settingFontFamily = Settings.GetSetting("ScriptEditorFontFamily");
            if (settingFontSize > 0)
            {
                scriptEditor.Font = new Font(settingFontFamily, settingFontSize);
            }

            // Set background colour according to settings
            scriptEditor.BackBrush = new SolidBrush(Helpers.GetColorFromString(Settings.GetSetting("ScriptEditorBackgroundColour")));

            this.Controls.Add(scriptEditor);
        }

        void scriptEditor_TextChanged(object sender, FastColoredTextBoxNS.TextChangedEventArgs e)
        {
            Range range = scriptEditor.Range;

            e.ChangedRange.ClearFoldingMarkers();

            e.ChangedRange.SetFoldingMarkers("{", "}");
            e.ChangedRange.SetFoldingMarkers(@"//region\b", @"//endregion\b");

            range.ClearStyle(CommentStyle);

            e.ChangedRange.ClearStyle(FuncStyle);
            e.ChangedRange.ClearStyle(AFuncStyle);
            e.ChangedRange.ClearStyle(NFuncStyle);
            e.ChangedRange.ClearStyle(StringStyle);
            e.ChangedRange.ClearStyle(IntStyle);
            e.ChangedRange.ClearStyle(BoolStyle);
            e.ChangedRange.ClearStyle(CommentStyle);

            e.ChangedRange.SetFoldingMarkers(@"//#region\b", @"//#endregion\b");

            e.ChangedRange.SetStyle(AFuncStyle, @"(\b)function(\b)", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(AFuncStyle, @"(\b)public(\b)", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(AFuncStyle, @"(\b)new(\b)", RegexOptions.Multiline);

            e.ChangedRange.SetStyle(FuncStyle, @"\b(function|new)\s+(?<range>[\w_]+?)\b", RegexOptions.Multiline);

            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)if\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)else\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)for\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)while\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)do\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)return\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)break\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)continue\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)switch\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)case\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)with\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)const\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)enum\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)@(\b)", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(NFuncStyle, @"(\b)SPC\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(BoolStyle, @"(\b)true\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(BoolStyle, @"(\b)false\b", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(BoolStyle, @"(\b)null\b", RegexOptions.Multiline);

            e.ChangedRange.SetStyle(IntStyle, @"(\b)pi(\b)|(\b)[0-9]+(\.[0-9]+)", RegexOptions.Multiline);
            e.ChangedRange.SetStyle(IntStyle, @"(\b)[0-9]+(\b)", RegexOptions.Multiline);

            range.SetStyle(CommentStyle, @"//.*$", RegexOptions.Multiline);
            range.SetStyle(CommentStyle, @"(/\*.*?\*/)|(/\*.*)", RegexOptions.Singleline);
            range.SetStyle(CommentStyle, @"(/\*.*?\*/)|(.*\*/)", RegexOptions.Singleline |
                        RegexOptions.RightToLeft);

            e.ChangedRange.SetStyle(StringStyle, "\"+(.*?)+\"", RegexOptions.Singleline |
                        RegexOptions.RightToLeft);

            try
            {
                scriptEditor.DefaultStyle.ForeBrush =
                    new SolidBrush(Helpers.GetColorFromString(Settings.GetSetting("ScriptEditorSynRegular")));
            }
            catch (Exception ex)
            {
                Errors.Log(ex);
                scriptEditor.DefaultStyle.ForeBrush = new SolidBrush(Color.Red);
            }
        }
    }
}
