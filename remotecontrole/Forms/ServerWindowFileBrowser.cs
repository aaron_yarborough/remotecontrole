﻿using OpenGraal.Core;
using OpenGraal.GraalIM;
using OpenGraal.GraalIM.Connections;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindowFileBrowser : DockContent
    {
        List<string> paths = new List<string>();

        public ServerWindowFileBrowser()
        {
            InitializeComponent();

            this.Shown += ServerWindowFileBrowser_Shown;
            this.treeViewListing.NodeMouseClick += treeViewListing_NodeMouseClick;
            this.FormClosing += ServerWindowFileBrowser_FormClosing;

            ImageList imageList = new ImageList();
            imageList.Images.Add("FolderIcon", Properties.Resources.filebrowsericon);
            treeViewListing.ImageList = imageList;
        }

        void ServerWindowFileBrowser_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            Information.Framework.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.RC_FILEBROWSER_END);
        }

        void treeViewListing_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Information.Framework.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.RC_FILEBROWSER_CD + e.Node.FullPath + "/");
        }

        void ServerWindowFileBrowser_Shown(object sender, EventArgs e)
        {
            Information.Framework.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.RC_FILEBROWSER_START + "\"\"");
        }

        delegate void CallbackToConsole(string text);
        delegate void CallbackAddDirectory(string text);
        delegate void CallbackFinishedLoadingDirectories();

        public void ToConsole(string text)
        {
            if (textBoxConsole.InvokeRequired)
            {
                this.Invoke(new CallbackToConsole(ToConsole), text);
            }

            try
            {
                textBoxConsole.AppendText(text + "\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine(text);
            }
        }

        public void AddDirectory(string path)
        {
            if (treeViewListing.InvokeRequired)
            {
                this.Invoke(new CallbackAddDirectory(AddDirectory), path);
            }

            try
            {
                paths.Add(path);
            }
            catch
            {
                return;
            }
        }

        public void FinishedLoadingDirectories()
        {
            if (treeViewListing.InvokeRequired)
            {
                this.Invoke(new CallbackFinishedLoadingDirectories(FinishedLoadingDirectories));
            }

            treeViewListing.PathSeparator = @"/";
            PopulateTreeView(paths.ToArray(), '/');
        }

        private void PopulateTreeView(string[] paths, char pathSeparator)
        {
            TreeNode lastNode = null;
            string subPathAgg;

            foreach (string path in paths)
            {
                subPathAgg = string.Empty;
                foreach (string subPath in path.Split(pathSeparator))
                {
                    subPathAgg += subPath + pathSeparator;
                    TreeNode[] nodes = treeViewListing.Nodes.Find(subPathAgg, true);
                    if (nodes.Length == 0)
                        if (lastNode == null)
                        {
                            lastNode = treeViewListing.Nodes.Add(subPathAgg, subPath);
                            lastNode.ImageIndex = 0;
                        }
                        else
                        {
                            lastNode = lastNode.Nodes.Add(subPathAgg, subPath);
                            lastNode.ImageIndex = 0;
                        }
                    else
                        lastNode = nodes[0];
                }
                lastNode = null; // This is the place code was changed

            }
        }
    }
}
