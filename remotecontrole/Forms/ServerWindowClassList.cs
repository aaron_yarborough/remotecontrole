﻿using OpenGraal.GraalIM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindowClassList : DockContent
    {
        public List<string> classList;

        public ServerWindowClassList()
        {
            InitializeComponent();

            this.FormClosing += ServerWindowClassList_FormClosing;
            this.listBoxClasses.MouseDoubleClick += listBoxClasses_MouseDoubleClick;
            classList = new List<string>();
        }

        void listBoxClasses_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string className = listBoxClasses.SelectedItem.ToString();
            Information.NcConnection.GetClassContent(className);
        }

        void ServerWindowClassList_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        delegate void AddClassCallback(string className);

        public void AddClass(string className)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AddClassCallback(AddClass), className);
            }

            try
            {
                listBoxClasses.Items.Add(className);
                listBoxClasses.Sorted = true;
            }
            catch { }
        }
    }
}
