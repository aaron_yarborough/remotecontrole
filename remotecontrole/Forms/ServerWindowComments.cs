﻿using OpenGraal.GraalIM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindowComments : DockContent
    {
        public string Account;

        public ServerWindowComments()
        {
            InitializeComponent();
        }

        delegate void CallbackOpenComments(string account, string text);

        public void OpenComments(string account, string text)
        {
            this.Account = account;
            this.Text = "Comments: " + account;
            this.fastColoredTextBoxComments.Text = text;
        }
    }
}
