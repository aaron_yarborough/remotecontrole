﻿using OpenGraal.Core;
using OpenGraal.GraalIM;
using OpenGraal.GraalIM.Connections;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindow : Form
    {
        public ServerWindowChat serverWindowChat;
        public ServerWindowPlayers serverWindowPlayers;
        public ServerWindowServerOptions serverWindowServerOptions;
        public ServerWindowWeaponList serverWindowWeaponList;
        public ServerWindowFileBrowser serverWindowFileBrowser;
        public ServerWindowClassList serverWindowClassList;
        public ServerWindowNPCList serverWindowNPCList;

        public ServerWindow()
        {
            
            InitializeComponent();
            
            panel1.Controls.Add(dockPanel);

            #region Event listeners
            this.FormClosed += ServerWindow_FormClosed;
            this.toolStrip1.ItemClicked += toolStrip1_ItemClicked;
            #endregion

            #region Interface

            serverWindowChat = new ServerWindowChat
            {
                CloseButton = false,
                CloseButtonVisible = false,
                Text = "Chat"
            };
            serverWindowChat.Show(dockPanel, DockState.Document);

            serverWindowPlayers = new ServerWindowPlayers
            {
                CloseButton = false,
                CloseButtonVisible = false,
                Text = "Players"
            };
            serverWindowPlayers.Show(dockPanel, DockState.DockLeft);

            serverWindowWeaponList = new ServerWindowWeaponList
            {
                Text = "Weapons"
            };

            serverWindowClassList = new ServerWindowClassList
            {
                Text = "Classes"
            };

            serverWindowServerOptions = new ServerWindowServerOptions();
            dockPanel.DockLeftPortion = 280;

            serverWindowFileBrowser = new ServerWindowFileBrowser();

            serverWindowNPCList = new ServerWindowNPCList();

            #endregion
        }

        void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "toolStripButtonOptions":
                    OptionsWindow optionsWindow = new OptionsWindow();
                    optionsWindow.Show();
                    break;

                case "toolStripButtonServerOptions":
                    Information.Framework.SendGSPacket(new CString() + (byte)GraalServer.PacketOut.RC_SERVEROPTIONSGET);

                    serverWindowServerOptions.Text = "Server Options";
                    serverWindowServerOptions.Show(dockPanel, DockState.Document);
                    break;
                
                case "toolStripButtonAppInfo":
                    MessageBox.Show(
                        "Version: " + Data.APP_VERSION + "\n" +
                        "Programmer: Aaron Yarborough\n" +
                        "\n" +
                        "With special thanks to:\n" +
                        "  OpenGraal Team: Libraries\n" +
                        "  Johnaudi: Programming help\n" +
                        "  Shurikan: App icon",
                        "Information"
                    );
                    break;

                case "toolStripButtonWeapons":
                    if (Information.Framework.ConnectedNC == false)
                    {
                        MessageBox.Show("You aren't connected to NC!", "Error");
                        return;
                    }

                    Information.NcConnection.GetWeaponList();
                    serverWindowWeaponList.Show(dockPanel, DockState.Document);
                    break;

                case "toolStripButtonClasses":
                    if (Information.Framework.ConnectedNC == false)
                    {
                        MessageBox.Show("You aren't connected to NC!", "Error");
                        return;
                    }

                    serverWindowClassList.Show(dockPanel, DockState.Document);
                    break;

                case "toolStripButtonDatabases":
                    if (Information.Framework.ConnectedNC == false)
                    {
                        MessageBox.Show("You aren't connected to NC!", "Error");
                        return;
                    }

                    serverWindowNPCList.Show(dockPanel, DockState.Document);
                    break;

                case "toolStripButtonFileBrowser":
                    serverWindowFileBrowser.Show(dockPanel, DockState.Document);
                    break;
            }
        }

        void ServerWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Information.Framework.CloseServers();
            Information.ServerListWindow.Show();
        }

        delegate void OpenCommentsHandler();
        public void OpenComments(string account, string text)
        {
            this.Invoke(new OpenWeaponHandler(delegate()
            {
                ServerWindowComments comments = new ServerWindowComments();
                comments.OpenComments(account, text);
                comments.Show(dockPanel, DockState.Document);
            }
            ));
        }

        delegate void OpenWeaponHandler();
        public void OpenWeapon(string weapon, string content)
        {
            this.Invoke(new OpenWeaponHandler(delegate()
                {
                    ServerWindowScriptEditor editor = new ServerWindowScriptEditor(weapon, ServerWindowScriptEditor.ScriptType.WEAPON);
                    editor.scriptName = weapon;
                    editor.SetText(content);
                    editor.Show(dockPanel, DockState.Document);
                }
            ));
        }

        delegate void OpenClassHandler();
        public void OpenClass(string className, string content)
        {
            this.Invoke(new OpenClassHandler(delegate()
                {
                    ServerWindowScriptEditor editor = new ServerWindowScriptEditor(className, ServerWindowScriptEditor.ScriptType.CLASS);
                    editor.scriptName = className;
                    editor.SetText(content);
                    editor.Show(dockPanel, DockState.Document);
                }
            ));
        }

        delegate void OpenDBHandler();
        public void OpenDB(int dbId, string content)
        {
            this.Invoke(new OpenDBHandler(delegate()
            {
                ServerWindowScriptEditor editor = new ServerWindowScriptEditor(serverWindowNPCList.DatabaseName, ServerWindowScriptEditor.ScriptType.NPC);
                editor.scriptName = serverWindowNPCList.DatabaseName;
                editor.scriptId = dbId;
                editor.SetText(content);
                editor.Show(dockPanel, DockState.Document);
            }
            ));
        }

        delegate void OpenPlayerHandler();
        public void OpenPlayer()
        {
            this.Invoke(new OpenPlayerHandler(delegate()
                {
                    ServerWindowPlayer playerWindow = new ServerWindowPlayer();
                    playerWindow.Text = "Player: ";
                    playerWindow.Show(dockPanel, DockState.Document);
                }
            ));
        }
    }
}
