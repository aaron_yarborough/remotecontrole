﻿using OpenGraal.Core;
using OpenGraal.GraalIM;
using OpenGraal.GraalIM.Connections;
using OpenGraal.NpcServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RemoteControlE
{
    public partial class ServerWindowNPCList : DockContent
    {
        public string DatabaseName;

        public ServerWindowNPCList()
        {
            InitializeComponent();

            ContextMenu ctx = new ContextMenu();
            ctx.MenuItems.Add(new MenuItem("Edit script"));
            ctx.MenuItems.Add(new MenuItem("Edit flags"));
            ctx.MenuItems.Add(new MenuItem("View attributes"));
            ctx.MenuItems.Add(new MenuItem("-"));
            ctx.MenuItems.Add(new MenuItem("Delete"));
            ctx.MenuItems.Add(new MenuItem("Warp"));
            ctx.MenuItems.Add(new MenuItem("Reset"));
            
            listViewNPCs.ContextMenu = ctx;

            this.FormClosing += ServerWindowNPCList_FormClosing;
            this.listViewNPCs.MouseDoubleClick += listViewNPCs_MouseDoubleClick;
        }

        void listViewNPCs_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            DatabaseName = listViewNPCs.SelectedItems[0].SubItems[0].Text;
            int dbId = Convert.ToInt32(listViewNPCs.SelectedItems[0].SubItems[3].Text);
            Information.NcConnection.SendPacket(new CString() + (byte)NCConnection.PacketIn.NC_NPCSCRIPTGET + dbId);
        }

        void ServerWindowNPCList_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        delegate void AddNPCHandler(string id, string name, string type, string level);
        public void AddNPC(string id, string name, string type, string level)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AddNPCHandler(AddNPC), id, name, type, level);
            }

            try
            {
                ListViewItem item = new ListViewItem(new string[] { name, type, level, id });
                listViewNPCs.Items.Add(item);
                listViewNPCs.Sorting = SortOrder.Ascending;
                listViewNPCs.Sort();
            }
            catch { }
        }
    }
}
