﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteControlE
{
    public static class Settings
    {
        public static Dictionary<string, string> GetSettings()
        {
            string json = System.IO.File.ReadAllText(System.IO.Path.Combine(Helpers.GetAppDataDir(), "data.settings.json"));
            Dictionary<string, string> settings = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            return settings;
        }

        public static string GetSetting(string setting)
        {
            Dictionary<string, string> settings = GetSettings();

            try
            {
                return settings[setting];
            }
            catch (Exception ex)
            {
                Errors.Log(ex);
                return null;
            }
        }

    }
}
