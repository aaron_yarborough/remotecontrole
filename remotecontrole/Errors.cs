﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RemoteControlE
{
    public static class Errors
    {
        public static void Log(Exception ex)
        {
            string path = Path.Combine(Helpers.GetAppDataDir(), "errorlog.txt");
            string content = String.Format("{0}\n\nMessage ---\n{1}\nHelp Link ---\n{2}\nSource ---\n{3}\nStack Trace ---\n{4}\nTarget Site ---\n{5}\n=========================\n",
                DateTime.Now,
                ex.Message,
                ex.HelpLink,
                ex.Source,
                ex.StackTrace,
                ex.TargetSite
            );

            File.AppendAllText(path, content);
        }

        public static void DumpFile(string filepath, string content)
        {
            string finalpath = Path.Combine(Helpers.GetAppDataDir(), filepath);
            try
            {
                File.WriteAllText(finalpath, content);
            }
            catch (Exception ex)
            {
                Errors.Log(ex);
            }
        }
    }
}
