﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteControlE.Models
{
    class ObjectBrowserFunction
    {
        public string Name { get; set; }
        public int Line { get; set; }
        public List<string> Params { get; set; }
    }
}
