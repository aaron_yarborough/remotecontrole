﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteControlE.Models
{
    class Login
    {
        public string Nickname { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
